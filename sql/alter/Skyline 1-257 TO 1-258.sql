# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.257');

# ---------------------------------------------------------------------- #
# Modify Table job                                                       #
# ---------------------------------------------------------------------- # 
ALTER TABLE `job`
	ADD INDEX `ServiceBaseModel` (`ServiceBaseModel`),
	ADD INDEX `ServiceAction` (`ServiceAction`);



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.258');
