{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Homepage"}
{$PageId = $HomePage}
{/block}

{block name=afterJqueryUI}
   <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
{/block}

{block name=scripts}
{*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*}
<link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />

{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*} 

<script type="text/javascript">  
    $(document).ready(function(){ 
        $('#unsentemail').change(function() {
            $('#sentemail').find("option").attr("selected", false);
            $('#EmailID').val($('#unsentemail').val());
            $('#SentEmailID').val('');
            $('#PrintEmailFlag').val('');
            
        });
        
        $('#sentemail').change(function() {
            $('#unsentemail').find("option").attr("selected", false);
            $('#EmailID').val('');
            $('#SentEmailID').val($('#sentemail').val());
            $('#PrintEmailFlag').val('');
        });
        
        $('#cancel_email_btn').click(function() {
            $('#EmailID').val('');
            $('#SentEmailID').val('');
            $('#PrintEmailFlag').val('');
        });
        
        
        $('#print_email_btn').click(function() {
            $('#PrintEmailFlag').val('1');
            $('#EMailManager').attr('action', "{$_subdomain}/index/printemail/{$JobID}");
            $('#EMailManager').attr('target', '_blank');
        });
        
        
        
    });

</script>    

{/block}

{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/index/jobsearch">Job Search</a> / <a href="{$_subdomain}/index/jobupdate/{$JobID}">Job Update</a> / Send EMail
    </div>
</div>
    
<div id="menu">
    
    {if $superadmin}
    
    {menu menu_items = [[$OutstandingJobsPage, 'Add&nbsp;Contact&nbsp;Note','','first',"$('#addContactHistory').trigger('click')"],
                        [$OverdueJobsPage,'Send&nbsp;email',"/index/sendemail/$JobID",''],
                        [$PerformancePage,'Send&nbsp;SMS',"/index/sendsms/$JobID",''],
                        [$LinksPage,'Cancel&nbsp;Job',"/index/canceljob/$JobID",''],
                        [$HomePage,'Other&nbsp;Actions',"/index/otheractions/$JobID",'']]}

    {else}
	
        {foreach $jobMenuItems as $jmkey => $jmvalue}
       
              {$jobMenuItems.$jmkey.2 = $jobMenuItems.$jmkey.2|cat:"/"|cat:$JobID}
              
        {/foreach}
	{menu menu_items = $jobMenuItems}
	
    {/if}
                        
                        
</div> 
    
    
    
<div class="main" id="jobdetails">
    
   
                                    
    <h2 class="span-8">Booking Details</h2>
    <h2 class="span-8" style="text-align: center;">{$ContactLastName}, {$ContactTitle} {$ContactFirstName}</h2>
    <h2 class="span-8 last" style="text-align: right;"><span style="font-size: small;">Skyline No:</span> SL{$JobID}</h2>
    
    <div class="span-24">
        <form id="CustomerDetails" name="CustomerDetails">
        <fieldset>
            <legend>Customer Details</legend>
            <p class="address">{$FullAddress}</p>
            <p class="contact-info">
                <span class="span-6 first"> <strong>(T)</strong> {$ContactHomePhone}   {if $ContactWorkPhoneExt neq ''}&nbsp;&nbsp;<strong>(EX)</strong> {$ContactWorkPhoneExt} {/if}</span> 
                <span class="span-6"> <strong>(M)</strong> {$ContactMobile} </span> 
                <span class="span-10 last"> <strong>(E)</strong> {$ContactEmail} </span>  

            </p>    
            {*
            <p class="row" style="text-align: right; margin-bottom: 0;">
                <a class="hide save" title="" href="#CustomerDetails">Save</a>
                <a class="colorbox" title="Edit Customer Details" href="{$_subdomain}/Popup/editCustomerDetails/{$CustomerID}">Edit</a>
            </p>*}
            
        </fieldset>
        </form>
    </div>
    <p>&nbsp;</p>
   
    
    <div class="span24">
        
         
        
        <form id="EMailManager" name="EMailManager" action="{$_subdomain}/index/sendemail/{$JobID}" method="post">
        <fieldset>
            <legend>Email Manager</legend>
            
             
            {if $errText}
                <p style="text-align:center;" id="errorText" >
                    <span class="centerInfoText" {if $errText neq '-1'} style="color:red;" {/if} >

                        {if $errText eq '-1'}
                            Email has been sent.
                        {else if $errText eq '1'}
                            Please select an email to send.
                        {else if $errText eq '2'}
                            Unable to send email.
                        {else if $errText eq '3'}
                             Email has been sent already.
                        {/if}

                    </span>
                </p>
            {/if}
            
            <!--To send email message to your customer select an option below.--->
            
            <p>
            
            <span class="span-11 first">
                Pre-defined Email Messages
                
                <select size="11" id="unsentemail" name="unsentemail">
                    {foreach $PredefinedEmails as $PredefinedEmail}
                        <option value="{$PredefinedEmail.EmailID}">{$PredefinedEmail.Title}</option>
                    {/foreach}
                </select><br><br>
                <input type="submit" name="send_email_btn" id="send_email_btn" class="btnStandard"  value="Send" >
                
                <input type="reset" name="cancel_email_btn" id="cancel_email_btn" class="btnCancel"  value="Cancel" >
            </span>
            <span class="span-11 last">
                Previously Sent Email Messages
                <select size="11" id="sentemail" name="sentemail">
                    {foreach $SentPredefinedEmails as $SentPredefinedEmail}
                        <option value="{$SentPredefinedEmail.EmailID}">{$SentPredefinedEmail.Title}</option>
                    {/foreach}
                </select><br><br>
                <input type="submit" name="resend_email_btn" id="resend_email_btn" class="btnStandard"  value="Re-Send" >
                
                <input type="submit" name="print_email_btn" id="print_email_btn" class="btnStandard"  value="Print" >
            </span>
            
            <p>  
                <input type="hidden" id="EmailID" name="EmailID">
                <input type="hidden" id="SentEmailID" name="SentEmailID" >
                <input type="hidden" id="PrintEmailFlag" name="PrintEmailFlag" >
                
                  
        </fieldset>    
        </form>
    </div>
 </div>
{/block}