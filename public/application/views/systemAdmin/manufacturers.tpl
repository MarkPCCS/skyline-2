{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $Manufacturers}
    {$fullscreen=true}
{/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
    .ui-combobox-input {
        width:300px;
    }
    </style>
{/block}

{block name=scripts}


    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
    <script type="text/javascript" src="{$_subdomain}/js/ajaxfileupload.js"></script>
 <script type="text/javascript" src="{$_subdomain}/js/ColReorder.js"></script>
    <script type="text/javascript">
         {if $SupderAdmin eq true} 
    $(document).ready(function() {
    
        $("#nId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/ProductSetup/manufacturers/'+urlencode($("#nId").val()));
            }
        });
        $("#cId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/ProductSetup/manufacturers/'+urlencode($("#nId").val())+'/'+urlencode($("#cId").val()));
            }
        });
    });
    {/if}
    function showTablePreferences(){
$.colorbox({ 
 
                       href:"{$_subdomain}/ProductSetup/tableDisplayPreferenceSetup/page=manufacturers/table=manufacturer",
                        title: "Table Display Preferences",
                        opacity: 0.75,
                        overlayClose: false,
                        escKey: false

                });
}
      
     
                 
    
                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    /**
    *  This is used to change color of the row if its in-active.
    */
    function inactiveRow(nRow, aData)
    {
          
        if (aData[3]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(2)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
            $('td:eq(2)', nRow).html( $statuses[0][0] );
        }
        
        
         //Getting assigned value for each Manufacturer for selected Network.
         
        if(aData[4])
        {
           
            
            
           $.post("{$_subdomain}/ProductSetup/manufacturers/getAssigned/"+urlencode(aData[4])+"/"+urlencode($("#nId").val())+"/"+urlencode($("#cId").val()),        

            '',      
            function(data){
                    var p = eval("(" + data + ")");
                    
                    if(p)
                    {   
                        $checked = ' checked="checked" ';
                    }
                    else
                    {
                        $checked = '  ';
                    }
                    $checkbox = '<input type="checkbox" class="checkBoxDataTable" name="assignedManufacturers[]" '+$checked+' value="'+aData[4]+'" > ';
                    $('td:eq(3)', nRow).html( $checkbox );

            });
        }
        
    }
    
    
    
    function ajaxFileUpload()
    {
           
            
            $.ajaxFileUpload
            (
                    {
                            url:'{$_subdomain}/ProductSetup/manufacturers/uploadImage/'+urlencode($("#UploadedManufacturerLogo").val()),
                            secureuri:false,
                            fileElementId:'ManufacturerLogo',
                            dataType: 'json',
                            data:{ name:'logan', id:'id' }
                           
                    }
            )

            return true;

    }
    
   
 

    $(document).ready(function() {
    
    
    
    
    
    

 var oTable = $('#ManufacturersResults').dataTable( {
"sDom": 'R<"left"><"top"f>t<"bottom"><"centered"><"right">pli<"clear">',
//"sDom": 'Rlfrtip',

"bPaginate": true,
"sPaginationType": "full_numbers",
"aLengthMenu": [[ 25, 50, 100 , -1], [25, 50, 100, "All"]],
"iDisplayLength" : 25,

"aoColumns": [ 
			
			
			{for $er=0 to $data_keys|@count-1}
                                {$vis=1}
                               
                               
                            {if $er==0}{$vis=0}{/if}
                            { "bVisible":{$vis} },
                               
			 
                           {/for} 
                               ////exception Tickbox
                            {if $nId!=''}
                                { "bVisible":true },
                                
                                {/if}
					 null
                            
		] 
   
 
        
          
});//datatable end
  $('#tt-Loader').hide();
  $('#ManufacturersResults').show();
   /* Add a click handler to the rows - this could be used as a callback */
	$("#ManufacturersResults tbody").click(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
                var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
                
    if (anSelected!="")  // null if we clicked on title row
    {
    
                $('#exportType').val(aData[0]);
                }
	});

 /* Get the rows which are currently selected */
function fnGetSelected( oTableLocal )
{
	var aReturn = new Array();
	var aTrs = oTableLocal.fnGetNodes();
	
	for ( var i=0 ; i<aTrs.length ; i++ )
	{
		if ( $(aTrs[i]).hasClass('row_selected') )
		{
			aReturn.push( aTrs[i] );
		}
	}
	return aReturn;
}



    /* Add a click handler for the edit row */
	$('button[id^=edit]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
$.colorbox({ 
 
                           href:"{$_subdomain}/ProductSetup/processManufacturer/id="+anSelected[0].id+"/nid={$nId}",
                        title: "Edit Manufacturer",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );                            
             
 /* Add a click handler for the delete row */
	$('button[id^=delete]').click( function() {
		var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
  //becouse all the data can be reordered and hidden any additinal info like ID needs to be stored in <tr> and accesed by anSelected[0].id where .id is data needet
if (confirm('Are you sure you want to delete this entry from database?')) {
    window.location="{$_subdomain}/ProductSetup/deleteManufacturer/id="+anSelected[0].id
} else {
    // Do nothing!
}


    }else{
    alert("Please select row first");
    }
		
	} );                            
             

/* Add a dblclick handler to the rows - this could be used as a callback */
	$("#ManufacturersResults  tbody").dblclick(function(event) {
		$(oTable.fnSettings().aoData).each(function (){
			$(this.nTr).removeClass('row_selected');
		});
		$(event.target.parentNode).addClass('row_selected');
            var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); // get datarow
    if (anSelected!="")  // null if we clicked on title row
    {
    
$.colorbox({ 
 
                        href:"{$_subdomain}/ProductSetup/processManufacturer/id="+anSelected[0].id+"/nid={$nId}",
                        title: "Edit Manufacturer",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });
    }else{
    alert("Please select row first");
    }
		
	} );  



$('a[id^=showpendingtick]').click( function() {
      
     
                
                              
		
                 {if isset($args['Pending'])&&$args['aprove']==Pending}
                         
                          window.location="{$_subdomain}/ProductSetup/manufacturers/";
                         {else}
                             
                              window.location="{$_subdomain}/ProductSetup/manufacturers/aprove=Pending";
                         {/if}
                 
	} );  
$('input[id^=unaprovedtick]').click( function() {
      
     
                
                     {if isset($args['aprove'])&&$args['aprove']==NotApproved}
                         
                          window.location="{$_subdomain}/ProductSetup/manufacturers/";
                         {else}
                             
                              window.location="{$_subdomain}/ProductSetup/manufacturers/aprove=NotApproved";
                         {/if}
		
                 
	} );  
$('input[id^=showSpUnmodified]').click( function() {
      
     
                
                     {if isset($args['showSpUnmodified'])&&$args['showSpUnmodified']==hideSpUnmodified}
                         
                          window.location="{$_subdomain}/ProductSetup/manufacturers/";
                         {else}
                             
                              window.location="{$_subdomain}/ProductSetup/manufacturers/showSpUnmodified=hideSpUnmodified";
                         {/if}
		
                 
	} );  
$('input[id^=inactivetick]').click( function() {
      
     
                
                     {if isset($args['status'])}
                         
                          window.location="{$_subdomain}/ProductSetup/manufacturers/";
                         {else}
                             
                              window.location="{$_subdomain}/ProductSetup/manufacturers/status=In-active";
                         {/if}
		
                 
	} );  




//help icons init
$('.helpTextIconQtip').each(function()
    {
        $HelpTextCode =  $(this).attr("id");
        // We make use of the .each() loop to gain access to each element via the "this" keyword...
        $(this).qtip(
        {
            hide: 'unfocus',
            events: {
         
                hide: function(){
               
                  $(this).qtip('api').set('content.text', '<img src="{$_subdomain}/images/ajax-loader.gif" '); // Direct API method 
                }
            },
            content: {
                // Set the text to an image HTML string with the correct src URL to the loading image you want to use
                text: '<img src="{$_subdomain}/images/ajax-loader.gif" >',
                ajax: {
                    url: '{$_subdomain}/Popup/helpText/' + urlencode($HelpTextCode) + '/Qtip=1/' + Math.random(),
                    once: false // Re-fetch the content each time I'm shown
                },
                title: {
                    text: 'Help', // Give the tooltip a title using each elements text
                    button: true
                }
            },
            position: {
                at: 'bottom center', // Position the tooltip above the link
                my: 'top center',
                viewport: $(window), // Keep the tooltip on-screen at all times
                effect: true // Disable positioning animation
            },
            show: {
                event: 'click',
               
                solo: true // Only show one tooltip at a time
            },
             
            style: {
                classes: 'qtip-tipped  qtip-shadow'
            }
        })
    })
 
    // Make sure it doesn't follow the link when we click it
    .click(function(event) {
        event.preventDefault()
    });


    });//doc ready end
function ManufacturerInsert()
{
$.colorbox({ 
 
                        href:"{$_subdomain}/ProductSetup/processManufacturer/",
                        title: "Insert Manufacturer",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}
function ManufacturerEdit()
{
    var anSelected = fnGetSelected( oTable );
                var aData = oTable.fnGetData(anSelected[0]); 
               
$.colorbox({ 
 
                        href:"{$_subdomain}/OrganisationSetup/processManufacturer/id="+aData,
                        title: "Edit Manufacturer",
                        opacity: 0.75,
                        width:800,
                        overlayClose: false,
                        escKey: false

                });

}

$(document).on('click', '#quick_find_btn', 
                                function() {

                    $.ajax( { type:'POST',
                            dataType:'html',
                            data:'postcode=' + $('#PostalCode').val(),
                            success:function(data, textStatus) { 

                                    if(data=='EM1011' || data.indexOf("EM1011")!=-1)
                                    {

                                            $('#selectOutput').html("<label class='fieldError' >error</label>"); 
                                            $("#selectOutput").slideDown("slow");
                                           

                                            $("#BuildingNameNumber").val('');	
                                            $("#Street").val('');
                                            $("#LocalArea").val('');
                                            $("#TownCity").val('');

                                            $('#PostalCode').focus();
                                    }
                                    else
                                    {
                                        var sow = $('#selectOutput').width();
                                        $('#selectOutput').html(data); 
                                        $("#selectOutput").slideDown("slow");
                                        $('#selectOutput').width(sow);
                                        $("#postSelect").focus();

                                    }




                            },
                            beforeSend:function(XMLHttpRequest){ 
                                   // $('#fetch').fadeIn('medium'); 
                                   $('*').css('cursor','wait');
                            },
                            complete:function(XMLHttpRequest, textStatus) { 
                                   // $('#fetch').fadeOut('medium') 
                                    setTimeout("$('#quickButton').fadeIn('medium')");
                                    $('*').css('cursor','default');
                            },
                            url:'{$_subdomain}/index/addresslookup' 
                        } ); 
                    return false;
                    
                    
                });   
                
                function addFields(selected) {	
                    var selected_split = selected.split(","); 	


                    $("#BuildingNameNumber").val(selected_split[0]).blur();	
                    $("#Street").val(selected_split[1]).blur();
                    $("#LocalArea").val(selected_split[2]).blur();
                    $("#TownCity").val(selected_split[3]).blur();
                    var spf = $("#SPFormFieldset").width();
                    $("#selectOutput").slideUp("slow"); 
                    $("#SPFormFieldset").width(spf);

                }
function filterTable(name){
$('input[type="text"]','#ManufacturersResults_filter').val(name);
e = jQuery.Event("keyup");
e.which = 13;
$('input[type="text"]','#ManufacturersResults_filter').trigger(e);


}

function saveAssigned()
{
$('#ManufacturersResultsForm').submit();
}




    </script>

    
{/block}


{block name=body}
 <style>
        .zindez10000{
            z-index:10000;
            }
    </style>
<div style="float:right">
         <a href="#" onclick="showTablePreferences();">Display Preferences</a>
            </div>
    <div class="breadcrumb" style="width:100%">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/productSetup" >{$page['Text']['product_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" style="width:100%">

               <div class="ServiceAdminTopPanel" >
                    <form id="ManufacturersTopForm" name="ManufacturersTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  

{if isset($pendingNo)&&$pendingNo>0}
                        <div style="text-align: center;background:#FFE8F1;border:1px solid red;padding: 4px;margin-top:10px;margin-bottom: 10px;width:300px;">
                            <a id="showpendingtick" href="#">Show Pending</a>
                            <span style="color:red">{$pendingNo}</span> {$page['Labels']['tableName']} Need Approval
                        </div>
                        {/if}
                  <div class="ServiceAdminResultsPanel" id="ManufacturersResultsPanel" >
                    
                      {if $SupderAdmin neq true} 
                      {* if user is not sa display unmodified checkbox*}
                      <div style="float:left;position: absolute"><input {if $showSpUnmodified===true} checked="checked"  {/if}type="checkbox" name="showSpUnmodified" id="showSpUnmodified" > Display Unmodified Records <img  src="{$_subdomain}/css/Skins/{$_theme}/images/information_icon.png" id="manufacturerShowUnmodifiedHelp" class="helpTextIconQtip"  title="Help" alt="help" width="15" height="15" ></div>
                       
                          
                      {/if}
                    {if $SupderAdmin eq true} 
                        <form id="nIdForm" class="nidCorrections">
                        {$page['Labels']['service_network_label']|escape:'html'}
                        <select name="nId" id="nId">			    
                            <option value="-1" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                            {foreach $networks as $network}

                                <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                        
                        <br /><span style="padding-right: 59px;">{$page['Labels']['client_label']|escape:'html'}</span>
                        <select {if $nId==""}style="display:none"{/if} name="cId" id="cId">
			    <option value="-1" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>

                            {foreach $clients as $client}

                                <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                       
                        
                        
                        
                        <span id="showAssignedPanel" {if $nId==''}style="display:none;"{/if} >
                        <input class="assignedTextBox" id="showAssigned" onclick="filterTable($(this).attr('checked'));"  type="checkbox" name="showAssigned"  value="1" {if $showAssigned neq ''} checked="checked" {/if}  /><span class="text" >{$page['Text']['show_assigned']|escape:'html'}</span> 
                        </span>
                        
                        </form>
                    {else}
                        
                        <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" >
                        <input type="hidden" name="cId" id="cId" value="{$cId|escape:'html'}" >
                        
                    {/if} 
                     {*<button onclick="window.location='{$_subdomain}/LookupTables/jobFaultCodes/'" style="position:absolute;margin-left: 500px" class="gplus-blue" type="button" id="JobFaultCodesButton">Job Fault Codes</button> *} 
                     <div style="text-align:center" id="tt-Loader"><img src="{$_subdomain}/images/ajax-loading_medium.gif"></div>
                    <form method="POST" action="{$_subdomain}/ProductSetup/saveAssigned" id="ManufacturersResultsForm" class="dataTableCorrections">
                        <table style="display:none" id="ManufacturersResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            
                                        {foreach from=$data_keys key=kk item=vv}
                                  
                                    <th>
                                        {$vv}
                                    </th>
                                   
                                  
                                {/foreach}
				
                            {if $nId!=''}
                                             <th>Assigned</th>
					     
                                             
                            {/if} 
			    
                             {if $cId!=''}
                                             
                                            
                            {/if} 
			    <th></th>
                                    </tr>
                            </thead>
                            <tbody>
                                {foreach $data as $d}
                             <tr id="{if isset($d['ServiceProviderManufacturerID'])}{$d['ServiceProviderManufacturerID']}{elseif isset($d['ManufacturerID'])}{$d['ManufacturerID']}{/if}">
                               {foreach from=$d key=kk item=vv}
                                   {if $kk!=csig&&$kk!="asig"}
                                   <td>
                                        {$vv}
                                    </td>				    
                                    {/if}
                                   {/foreach}
				   
                            {if $nId!='' }
                                <td>
                                    {if $cId!=""}
                                         {if $d['csig']=="Active"}<input value="{$d['ManufacturerID']}" name="assignedMan[]" type="checkbox" checked="checked">{else}<input value="{$d['ManufacturerID']}" name="assignedMan[]" type="checkbox">{/if}
                                        {else}
                                {if $d['asig']=="Active"}<input value="{$d['ManufacturerID']}" name="assignedMan[]" type="checkbox" checked="checked">{else}<input name="assignedMan[]" value="{$d['ManufacturerID']}" type="checkbox">{/if}
                                       {/if}
				  
                                </td>

				
                            {/if}
			   	{* Updated by praveen kumar N*}
				<td align="center"> <input type="checkbox" name="temp_chk" id="temp_chk"> </td>
				{* end *}
                             </tr>
                            {/foreach}
                            
                            </tbody>
                        </table> 
                            <div style="width:100%;text-align: right"><input id="inactivetick"  type="checkbox"  {if isset($args['status'])} checked=checked{/if}> Show Inactive&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
                                                               {if $SupderAdmin=="1"} <input id="unaprovedtick"  type="checkbox"  {if isset($args['aprove'])&&$args['aprove']==NotApproved}checked=checked{/if} > Show Unapproved {/if}
                    </div>
                            <input type="hidden" name="nid" value="{$nId}">
                            <input type="hidden" name="cid" value="{$cId}">
                     </form>
                </div>        

                  <div class="bottomButtonsPanelHolder" style="position: relative;">
                    <div class="bottomButtonsPanel" style="position:absolute;top:-65px;width:200px">
                            <button type="button" id="edit"  class="gplus-blue">Edit</button>
                            <button type="button" onclick="ManufacturerInsert()" class="gplus-blue">Insert</button>
                            <button type="button" id="delete" class="gplus-red">Delete</button>
                    </div>
                </div>  
                 <div class="centerInfoText" id="centerInfoText" style="display:none;" ></div>
               

                {if $SupderAdmin eq false} 

                   <input type="hidden" name="addButtonId" id="addButtonId" > 

                {/if} 
               


    </div>
                {if $nId!='' }		
                <div>
                <hr>
                 <button type="button" class="gplus-blue" onclick="saveAssigned()">Save</button>
                </div> 		              
                   {/if}     

 <div>
                <hr>
                <button class="gplus-blue" style="float:right" type="button" onclick="window.location='{$_subdomain}/SystemAdmin/index/productSetup'">Finish</button>
                </div>        

{/block}



