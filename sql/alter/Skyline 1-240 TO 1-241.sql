
# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.240');


# ---------------------------------------------------------------------- #
# Changes for Andris' Part Codes                                         #
# ---------------------------------------------------------------------- #


ALTER TABLE manufacturer ADD COLUMN ApproveStatus ENUM('Approved','Pending','NotApproved') NULL DEFAULT 'Approved';

ALTER TABLE model ADD COLUMN ApproveStatus ENUM('Approved','Pending','NotApproved') NULL DEFAULT 'Approved';

CREATE TABLE part_section_code ( PartSectionCodeID INT(11) NOT NULL AUTO_INCREMENT, PartSectionCode CHAR(1) NOT NULL, PartSectionCodeDescription VARCHAR(100) NOT NULL, PartSectionCodeComment VARCHAR(250) NULL DEFAULT NULL, ModifiedUserID INT(11) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', PRIMARY KEY (PartSectionCodeID), INDEX user_TO_PartSection_codes (ModifiedUserID), CONSTRAINT user_TO_PartSection_codes FOREIGN KEY (ModifiedUserID) REFERENCES user (UserID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

CREATE TABLE part_defect_code ( PartDefectCodeID INT(11) NOT NULL AUTO_INCREMENT, PartDefectCode CHAR(1) NOT NULL, PartDefectCodeDescription VARCHAR(100) NOT NULL, PartDefectCodeComment VARCHAR(250) NULL DEFAULT NULL, ModifiedUserID INT(11) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', PRIMARY KEY (PartDefectCodeID), INDEX user_TO_PartDefect_codes (ModifiedUserID), CONSTRAINT user_TO_PartDefect_codes FOREIGN KEY (ModifiedUserID) REFERENCES user (UserID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

CREATE TABLE part_repair_code ( PartRepairCodeID INT(11) NOT NULL AUTO_INCREMENT, PartRepairCode CHAR(1) NOT NULL, PartRepairCodeDescription VARCHAR(100) NOT NULL, PartRepairCodeComment VARCHAR(250) NULL DEFAULT NULL, ModifiedUserID INT(11) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, Status ENUM('Active','In-active') NOT NULL DEFAULT 'Active', PRIMARY KEY (PartRepairCodeID), INDEX user_TO_PartRepair_codes (ModifiedUserID), CONSTRAINT user_TO_PartRepair_codes FOREIGN KEY (ModifiedUserID) REFERENCES user (UserID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

CREATE TABLE part_section_code_subset ( PartSectionCodeID INT(11) NOT NULL, ManufacturerID INT(11) NOT NULL, RepairSkillID INT(11) NOT NULL, ModifiedUserID INT(11) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, INDEX manufacturer_TO_part_section_codes (ManufacturerID), INDEX repair_skill_TO_part_section_codes (RepairSkillID), INDEX user_TO_part_section_codes (ModifiedUserID), INDEX part_section_code_TO_part_section_code_subset (PartSectionCodeID), CONSTRAINT part_section_code_TO_part_section_code_subset FOREIGN KEY (PartSectionCodeID) REFERENCES part_section_code (PartSectionCodeID), CONSTRAINT manufacturer_TO_part_section_code_subset FOREIGN KEY (ManufacturerID) REFERENCES manufacturer (ManufacturerID), CONSTRAINT repair_skill_TO_part_section_code_subset FOREIGN KEY (RepairSkillID) REFERENCES repair_skill (RepairSkillID), CONSTRAINT user_TO_part_section_code_subset FOREIGN KEY (ModifiedUserID) REFERENCES user (UserID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

CREATE TABLE part_defect_code_subset ( PartDefectCodeID INT(11) NOT NULL, ManufacturerID INT(11) NOT NULL, RepairSkillID INT(11) NOT NULL, ModifiedUserID INT(11) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, INDEX manufacturer_TO_part_defect_codes (ManufacturerID), INDEX repair_skill_TO_part_defect_codes (RepairSkillID), INDEX user_TO_part_defect_codes (ModifiedUserID), INDEX part_defect_code_TO_part_defect_code_subset (PartDefectCodeID), CONSTRAINT part_defect_code_TO_part_defect_code_subset FOREIGN KEY (PartDefectCodeID) REFERENCES part_defect_code (PartDefectCodeID), CONSTRAINT manufacturer_TO_part_defect_code_subset FOREIGN KEY (ManufacturerID) REFERENCES manufacturer (ManufacturerID), CONSTRAINT repair_skill_TO_part_defect_code_subset FOREIGN KEY (RepairSkillID) REFERENCES repair_skill (RepairSkillID), CONSTRAINT user_TO_part_defect_code_subset FOREIGN KEY (ModifiedUserID) REFERENCES user (UserID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

CREATE TABLE part_repair_code_subset ( PartRepairCodeID INT(11) NOT NULL, ManufacturerID INT(11) NOT NULL, RepairSkillID INT(11) NOT NULL, ModifiedUserID INT(11) NULL DEFAULT NULL, ModifiedDate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, INDEX manufacturer_TO_part_repair_codes (ManufacturerID), INDEX repair_skill_TO_part_repair_codes (RepairSkillID), INDEX user_TO_part_repair_codes (ModifiedUserID), INDEX part_repair_code_TO_part_repair_code_subset (PartRepairCodeID), CONSTRAINT part_repair_code_TO_part_repair_code_subset FOREIGN KEY (PartRepairCodeID) REFERENCES part_repair_code (PartRepairCodeID), CONSTRAINT manufacturer_TO_part_repair_code_subset FOREIGN KEY (ManufacturerID) REFERENCES manufacturer (ManufacturerID), CONSTRAINT repair_skill_TO_part_repair_code_subset FOREIGN KEY (RepairSkillID) REFERENCES repair_skill (RepairSkillID), CONSTRAINT user_TO_part_repair_code_subset FOREIGN KEY (ModifiedUserID) REFERENCES user (UserID) ) COLLATE='latin1_swedish_ci' ENGINE=InnoDB;

ALTER TABLE part ADD COLUMN PartSectionCodeID INT(11) NULL DEFAULT NULL AFTER SectionCode, ADD COLUMN PartDefectCodeID INT(11) NULL DEFAULT NULL AFTER DefectCode, ADD COLUMN PartRepairCodeID INT(11) NULL DEFAULT NULL AFTER RepairCode;

 
 
# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.241');
