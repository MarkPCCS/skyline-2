# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.293');

# ---------------------------------------------------------------------- #
# Add remote_engineer_history table										 #
# ---------------------------------------------------------------------- # 
CREATE TABLE `remote_engineer_history` (
	`RemoteEngineerHistoryID` INT(10) NOT NULL AUTO_INCREMENT,
	`AppointmentID` INT(10) NOT NULL DEFAULT '0',
	`SBJobNo` INT(10) NOT NULL DEFAULT '0',
	`Date` DATE NULL DEFAULT NULL,
	`Time` TIME NULL DEFAULT NULL,
	`Action` VARCHAR(50) NOT NULL,
	`Notes` VARCHAR(500) NULL,
	`URL` VARCHAR(100) NOT NULL DEFAULT '0',
	`Latitude` DECIMAL(11,8) NOT NULL DEFAULT '0',
	`Longitude` DECIMAL(11,8) NOT NULL DEFAULT '0',
	PRIMARY KEY (`RemoteEngineerHistoryID`),
	INDEX `AppointmentID` (`AppointmentID`),
	INDEX `SBJobNo` (`SBJobNo`),
	INDEX `Action` (`Action`),
	CONSTRAINT `appointment_TO_remote_engineer_history` FOREIGN KEY (`AppointmentID`) REFERENCES `appointment` (`AppointmentID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.294');
