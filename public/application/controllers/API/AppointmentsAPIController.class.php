<?php

/**
 * AppointmentsAPIContoller.class.php
 * 
 * Implementation of Appointments API for SkyLine
 * Part of Skyline Appointments Project
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 - 2013 PC Control Systems
 * @link       
 * @version    1.07
 * 
 * Changes
 * Date        Version Author                Reason
 * 15/10/2012  1.00    Andrew J. Williams    Initial Version
 * 24/10/2012  1.01    Andris Polnikovs      draft 50
 * 24/10/2012  1.02    Andris Polnikovs      draft 51
 * 15/11/2012  1.03    Andrew J. Williams    BRS 114 - Servicebase Cancel Job routeine to delete appointments
 * 16/11/2012  1.04    Andrew J. Williams    Issue 136 - DeleteAppointmentsForCancelledJob API should be GET not POST
 * 21/11/2012  1.05    Andrew J. Williams    Response not being passed to Servicebase on Cancel Job
 * 14/12/2012  1.05A   Andrew J. Williams    Added PutCompleteAppointment
 * 12/03/2013  1.06    Andris Polnikovs      Allow bulk import 1 appointment
 * 20/03/2013  1.07    Andris Polnikovs      Whirpool fix
 * 26/03/2013  1.08    Andrew J. Williams    Issue 281 - AppointmentAPI error in log file
 * ****************************************************************************** */
require_once('CustomRESTController.class.php');
include_once(APPLICATION_PATH . '/controllers/API/SkylineAPI.class.php');
include_once(APPLICATION_PATH . '/include/xml2array.php');

class AppointmentsAPI extends SkylineAPI {

    public $debug = false;
    private $api_appointment_fields = array(
        'SLNumber' => array('field' => 'JobID', 'type' => 'Integer'),
        'SLAppointmentID' => array('field' => 'AppointmentID', 'type' => 'Integer'),
        'SBAppointmentID' => array('field' => 'SBAppointID', 'type' => 'Integer'),
        'Notes' => array('field' => 'Notes', 'type' => 'String', 'length' => 1000),
        'AppointmentDate' => array('field' => 'AppointmentDate', 'type' => 'Date'),
        'AppointmentTime' => array('field' => 'ViamenteStartTime', 'type' => 'DateTime'),
        'AppointmentType' => array('field' => 'AppointmentType', 'type' => 'String', 'length' => 30),
        'TimeSlot' => array('field' => 'AppointmentTime', 'type' => 'String', 'length' => 10),
        'OutCardLeft' => array('field' => 'OutCardLeft', 'type' => 'enum', 'enums' => array('Y' => "1", 'N' => "0")),
        'EngineerCode' => array('field' => 'EngineerCode', 'type' => 'String', 'length' => 3),
        'CompletionEngineerUserCode' => array('field' => 'CompletionEngineerUserCode', 'type' => 'String', 'length' => 3),
        'ArrivalDateTime' => array('field' => 'ArrivalDateTime', 'type' => 'DateTime'),
        'DepartDateTime' => array('field' => 'DepartDateTime', 'type' => 'DateTime'),
        'PartsAttached' => array('field' => 'PartsAttached', 'type' => 'Integer'),
        'CompletionDateTime' => array('field' => 'DepartDateTime', 'type' => 'DateTime')
    );
    private $api_nonskylinejob_fields = array(
        'SBJobNo' => array('field' => 'ServiceProviderJobNo', 'type' => 'Integer'),
        'SLAppointmentID' => array('field' => 'AppointmentID', 'type' => 'Integer'),
        'CustSurname' => array('field' => 'CustomerSurname', 'type' => 'String', 'length' => 40),
        'CustTitle' => array('field' => 'CustomerTitle', 'type' => 'String', 'length' => 10),
        'CustPostcode' => array('field' => 'Postcode', 'type' => 'String', 'length' => 8),
        'CustAddress1' => array('field' => 'CustomerAddress1', 'type' => 'String', 'length' => 50),
        'CustAddress2' => array('field' => 'CustomerAddress2', 'type' => 'String', 'length' => 50),
        'CustAddress3' => array('field' => 'CustomerAddress3', 'type' => 'String', 'length' => 50),
        'CustAddress4' => array('field' => 'CustomerAddress4', 'type' => 'String', 'length' => 50),
        'ServiceType' => array('field' => 'ServiceType', 'type' => 'String', 'length' => 50),
        'ProductType' => array('field' => 'ProductType', 'type' => 'String', 'length' => 50),
        'Manufacturer' => array('field' => 'Manufacturer', 'type' => 'String', 'length' => 50),
        'ModelNumber' => array('field' => 'ModelNumber', 'type' => 'String', 'length' => 50),
    );

    /**
     * Description
     * 
     * Deal with post  
     * 
     * @param array $args   Associative array of arguments passed                    
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function get($args) {
        if ($this->debug)
            $this->log("AppointmentsAPIController::get  " . var_export($args, true), 'appointments_api_');

        switch (count($args)) {

            case 2:
                if (isset($args['CancelJob']) && isset($args['SBJobNo'])) {
                    if ($this->debug)
                        $this->log("AppointmentsAPIController::get  CancelJob={$args['CancelJob']} SBJobNo={$args['SBJobNo']}", 'appointments_api_');
                    $response = $this->DeleteAppointmentsForCancelledJob($args['SBJobNo']);
                    $httpcode = 200;
                } else {
                    $this->sendResponse(406);
                    exit();
                }
                break;

            default:
                $this->sendResponse(501);
                exit;
        }
        $this->sendResponse($httpcode, $response);
    }

    /**
     * Description
     * 
     * Deal with post  
     * 
     * @param array $args   Associative array of arguments passed                    
     * 
     * @return 
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function post($args) {

        if ($this->debug)
            $this->log("AppointmentsAPIController::post  " . var_export(@file_get_contents('php://input'), true), 'appointments_api_');

        switch (count($args)) {

            default:
                /* No arguments */
                $params = xml2array(@file_get_contents('php://input'));         /* Get XML payload body */

                if (isset($params['Appointments']['Appointment'][0]['CompleteAppointment']) && $params['Appointments']['Appointment'][0]['CompleteAppointment'] != '') {
                    $response = $this->PutCompleteAppointment($params);
                } else {
                    $response = $this->PutAppointmentsBulkImport($params);
                }

                $httpcode = 200;
                break;
        }
        $this->sendResponse($httpcode, $response);
    }

    /**
     * Validate parameters passed to API
     * 
     * @param array $params     Associative array of fields to be validated 
     *        array $table      Associative array of paramters to db mapping
     * 
     * @return validated_args   Fields taken from prameter matched to table fields
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    public function validate_args($params, $table) {
        $validated = array();                                                   /* Validated fields */

        foreach ($params as $pf => $pv) {
            if (isset($table[$pf]) OR isset($table[strtoupper($pf)])) {
                switch (strtolower($table[$pf]['type'])) {
                    case 'string':
                        $pv = str_replace("CRLF", "\r\n", $pv);
                        if (isset($table[$pf]['length'])) {
                            $validated[$table[$pf]['field']] = substr((string) $pv, 0, urldecode($table[$pf]['length']));
                        } else {
                            $validated[$table[$pf]['field']] = (string) urldecode($pv);
                        }
                        break;

                    case 'date':
                        if (!is_null($pv)) {
                            if ($pv != 0) {
                                $validated[$table[$pf]['field']] = date('Y-m-d', strtotime(str_replace('/', '.', $pv)));
                            } /* fi $pv != 0 */
                        } /* fi ! is_null ($pv) */
                        break;

                    case 'time':
                        $validated[$table[$pf]['field']] = date('H:i:s', strtotime($pv));
                        break;

                    case 'datetime':
                        $validated[$table[$pf]['field']] = date('Y-m-d H:i:s', strtotime($pv));
                        break;

                    case 'decimal':
                        $validated[$table[$pf]['field']] = (float) $pv;
                        break;

                    case 'integer':
                        $validated[$table[$pf]['field']] = intval($pv);
                        break;

                    case 'enum':
                        foreach ($table[$pf]['enums'] as $i => $e) {
                            if (strtolower($i) == strtolower($pv)) {
                                $validated[$table[$pf]['field']] = $e;
                            }
                        }
                        break;

                    default:
                        $validated[$table[$pf]['field']] = $pv;
                }
            }
        }
        return($validated);
    }

    /**
     * Skyline API  Servicebase.PutAppointmentsBulkImport
     *  
     * Allow bulk import of appointments from servicebase
     * 
     * @param Asscoaiative array                 
     * 
     * @return    Associative array containing respose
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  (First Draft)
     *         Andris Polnikovs <a.polnikovs@pccsuk.com> (Later Version)
     * ************************************************************************ */
    private function PutAppointmentsBulkImport($params) {

        if ($this->debug)
            $this->log("AppointmentsAPIController::PutAppointmentsBulkImport - args" . var_export($params, true), 'appointments_api_');

        if (!isset($params['Appointments']['Appointment']['SBJobNo'])) {
            $input = $params['Appointments'];
            $text = "Appointment";
        } else {
            $input = $params;
            $text = "Appointments";
        }
        $returnMessage = array();

        $appointment_model = $this->loadModel('Appointment');
        $job_model = $this->loadModel('Job');
        $non_skyline_job_model = $this->LoadModel('NonSkylineJob');
        $skillset_model = $this->loadModel('Skillset');
        $spe_model = $this->loadModel('ServiceProviderEngineer');
        $spss_model = $this->loadModel('ServiceProviderSkillsSet');
        $users_model = $this->loadModel('Users');
        $diary = $this->loadModel('Diary');

        $spId = $users_model->getServiceProviderID($this->user->UserID);

        $diary->deleteRMAAppointments($spId);
        if ($this->debug)
            $this->log("AppointmentsAPIController::end" . var_export($params, true), 'deleteRMAappointments_');
        $n = 0;
        if (sizeof($input) == 1) {
            
        }

        foreach ($input[$text] as $app) {
            $n++;
            if (isset($app['AppointmentDate'])) { /* Check if appointment date is set - Issue 218 */
                if (!isset($app['AppointmentTime'])) {
                    $app['AppointmentTime'] = "00:00:00";
                }
                $appTempTime = explode(' ', $app['AppointmentTime']);
                if (isset($appTempTime[0])) {
                    $app['AppointmentTime'] = $appTempTime[0];
                }

                if (isset($appTempTime[1])) {
                    $app['TimeSlot'] = $appTempTime[1];
                }
                $app['AppointmentTime'] = date('Y-m-d', strtotime(str_replace('/', '.', $app['AppointmentDate']))) . ' ' . $app['AppointmentTime'];

                if ($this->debug)
                    $this->log("AppointmentsAPIController::PutAppointmentsBulkImport - Appointment Parmas " . var_export($app, true), 'appointments_api_');

                if (isset($app['SBJobNo'])) {
                    $returnMessage['Appointments'][$n]['Appointment']['SBJobNo'] = $app['SBJobNo'];
                }

                if (isset($app['SBAppointmentId'])) {
                    $returnMessage['Appointments'][$n]['Appointment']['SBJobNo']['SBAppointmentID'] = $app['SBAppointmentID'];
                }

                if (isset($app['RMANumber']) && !is_null($app['RMANumber']) && $app['RMANumber'] != "") {
                    $app['SLNumber'] = $job_model->getIdFromRmaNo($app['RMANumber']);
                    $returnMessage['Appointments'][$n]['Appointment']['RMANumber'] = $app['RMANumber'];
                } /* fi isset $app['RMANumber'] */
                if ($app['SkillType'] == "") {
                    $app['SkillType'] = 2; //if no skill type assume brown goods
                }
                $app['AppointmentType'] = $diary->AppTypeName($app['AppointmentType']);
                if ($app['AppointmentType'] == "") {
                    $app['AppointmentType'] = 5; //if no atype type assumerepair
                }
                //whirpool fix
                if (isset($app['Client']) && $app['Client'] == 'WPL') {

                    $app['SkillType'] = 4; //skill type assume white goods

                    $app['AppointmentType'] = 5; //if no atype type assumerepair


                    if ($app['AppointmentType'] == "FIT SPARE") {
                        $app['AppointmentType'] = 15; //fit parts
                    }
                }
                //whirpool end fix
                // if ($this->debug) $this->log("Repair_skill=()".$diary->repairSkillName($app['SkillType'])."Appointment type=".$diary->AppTypeName($app['AppointmentType']),"skillset_request_");


                $skillset_fields = $skillset_model->getSkillset($app['SkillType'], $app['AppointmentType']);
                if ($this->debug)
                    $this->log("skillid=" . $skillset_fields['SkillsetID'], "skillset_request_");
                if ($this->debug)
                    $this->log($app['SkillType'] . " " . $app['AppointmentType'], "skillset_request_");
                if (is_null($skillset_fields)) {
                    if ($this->debug)
                        $this->log("AppointmentsAPIController::PutAppointmentsBulkImport - Skillset {$app['SkillType']} not found for SB Appointment No {$app['SBAppointmentID']} (SB Job {$app['SBJobNo']})", 'appointments_api_');
                    $this->log("AppointmentsAPIController::PutAppointmentsBulkImport - ERROR Skillset {$app['SkillType']} not found for SB Appointment No {$app['SBAppointmentID']} (SB Job {$app['SBJobNo']})");
                    $returnMessage['Appointments'][$n]['Appointment']['Message'] = "Skillset {$app['SkillType']} not found for SB Appointment No {$app['SBAppointmentID']} (SB Job {$app['SBJobNo']})";

                    break;
                }

                $uId = $this->user->UserID;

                $appointment_fields = $this->validate_args($app, $this->api_appointment_fields);    /* Validate appointment fields */
                $appointment_fields['ServiceProviderSkillsetID'] = $spss_model->getIdByServiceProviderSkillset($skillset_fields['SkillsetID'], $spId);
                $appointment_fields['Duration'] = $skillset_fields['ServiceDuration'];
                $appointment_fields['MenRequired'] = $skillset_fields['EngineersRequired'];
                $appointment_fields['ServiceProviderID'] = $spId;
                $appointment_fields['CreatedUserID'] = $uId;
                $appointment_fields['UserID'] = $uId;
                $appointment_fields['ServiceProviderEngineerID'] = $spe_model->getIdByCode($app['EngineerCode'], $spId);

                if ($this->debug)
                    $this->log('appointments_fields' . var_export($appointment_fields, true), 'appointments_api_');

                if (is_null($appointment_fields['ServiceProviderEngineerID'])) {
                    if ($this->debug)
                        $this->log("AppointmentsAPIController::PutAppointmentsBulkImport - Engineer code {$app['EngineerCode']} not found for SB Appointment No {$app['SBAppointmentID']} (SB Job {$app['SBJobNo']})", 'appointments_api_');
                    $this->log("AppointmentsAPIController::PutAppointmentsBulkImport - ERROR Engineer code {$app['EngineerCode']} not found for SB Appointment No {$app['SBAppointmentID']} (SB Job {$app['SBJobNo']})");
                    //  $returnMessage['Appointments'][$n]['Appointment']['Message'] = "Engineer code {$app['EngineerCode']} not found for SB Appointment No {$app['SBAppointmentID']} (SB Job {$app['SBJobNo']})";
                    //break; can be null doesnot mattaer viamente overeide anyway
                }
                $appointment_fields = array_merge($appointment_fields, $app);

                $appointment_fields['username'] = $this->user->Name;

                $resp = $diary->insertAppointment(null, null, null, $appointment_fields);

                if (!isset($resp['SLNumber'])) {
                    $sljobno = null;
                } else {
                    $sljobno = $resp['SLNumber'];
                }
                $returnMessage['Appointments'][$n]['Appointment']['SLAppointmentID'] = $resp['appointmentId'];
                $returnMessage['Appointments'][$n]['Appointment']['Message'] = $resp['status'];
                $returnMessage['Appointments'][$n]['Appointment']['SBAppointmentID'] = $appointment_fields['SBAppointmentID'];
                $returnMessage['Appointments'][$n]['Appointment']['SLNumber'] = $sljobno;
                $returnMessage['Appointments'][$n]['Appointment']['SBJobNo'] = $appointment_fields['SBJobNo'];
            } else {
                $returnMessage['Appointments'][$n]['Appointment']['Message'] = 'No appointment date';
            } /* fi (isset $app['AppointmentDate'] */
        }

        return($returnMessage);
    }

    /**
     * Skyline API  Servicebase.PutCompleteAppointment
     *  
     * Allow servicebase to update the completed job status
     * 
     * @param Asscoaiative array                 
     * 
     * @return    Associative array containing respose
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>
     * ************************************************************************ */
    private function PutCompleteAppointment($params) {
        if ($this->debug)
            $this->log("AppointmentsAPIController::PutAppointmentsBulkImport - args" . var_export($params, true), 'appointments_api_');
        $input = $params['Appointments'];
        $returnMessage = array();

        $appointment_model = $this->loadModel('Appointment');
        $diary = $this->loadModel('Diary');


        $n = 0;

        foreach ($input['Appointment'] as $app) { /* Loop through each appointment */
            $n++;

            $appointment_fields = array();
            $appointment_fields = $this->validate_args($app, $this->api_appointment_fields);    /* Validate appointment fields */
            if ($app['CompleteAppointment'] == 1)
                $appointment_fields['CompletedBy'] = 'Remote Engineer';
        }
    }

    /**
     * Skyline API  Servicebase.DeleteAppointmentsForCancelledJob
     *  
     * Delete the appointments for a job the Servicebase has cancelled
     * 
     * @param integer $sbJobNo      Servicebase Job Number               
     * 
     * @return    Associative array containing respose
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     * ************************************************************************ */
    private function DeleteAppointmentsForCancelledJob($sbJobNo) {
        $appointment_model = $this->loadModel('Appointment');
        $contact_history_model = $this->loadModel('ContactHistory');

        if (!isset($this->user->ServiceProviderID)) { /* If the user does not have a service provider set */
            $this->log('No service Provider ID set', 'apppointments_api_');

            $response = array(
                'Code' => "SC0006",
                'Response' => "No service Provider ID set",
                'ResponseCode' => "SC0006",
                'ResponseDetails' => "No service Provider ID set",
                'User' => $this->user->UserID
            );
            return($response);
        }

        $result = $appointment_model->getSBJobAppointmentList($sbJobNo, $this->user->UserID);

        if ($this->debug)
            $this->log("APIAppointments::DeleteAppointmentsForCancelledJob - Query Result \n" . var_export($result, true), 'appointments_api_');

        $appresp = array();
        $success = true;

        for ($n = 0; $n < count($result); $n++) {
            if (!is_null($result[$n]['JobID'])) { /* If we have a JobID */
                $ch = array(/* We want to write contact history */
                    'JobID' => $result[$n]['JobID'],
                    'ContactHistoryActionID' => 14,
                    'ContactDate' => date('Y-m-d'),
                    'ContactTime' => date('H:i:s'),
                    'Note' => "Appointment for {$result[$n]['AppointmentDate']} cancelled due to Servicebase cancelling job"
                );

                $chResult = $contact_history_model->create($ch);

                if ($this->debug)
                    $this->log("APIAppointments::DeleteAppointmentsForCancelledJob - Contact History create result \n" . var_export($chResult, true), 'appointments_api_');
            } /* fi is_null $result[$n]['JobID'] */

            $appresp[$n] = $appointment_model->delete(array('AppointmentID' => $result[$n]['AppointmentID']));
            if ($appresp[$n]['status'] == 'FAIL')
                $success = false;

            if ($this->debug)
                $this->log("APIAppointments::DeleteAppointmentsForCancelledJob - Delete Appointment result $n \n" . var_export($appresp[$n]['status'], true), 'appointments_api_');
        } /* next n */

        if ($success) { /* If successful */
            $response = array(/* Return success response */
                'ResponseCode' => 'SC0001',
                'ResponseDescription' => 'Success'
            );
        } else {
            $response = array(/* Return success response */
                'ResponseCode' => 'SC0002',
                'ResponseDescription' => 'Failure',
                'Information' => $appresp
            );
        }

        if ($this->debug)
            $this->log("APIAppointments::DeleteAppointmentsForCancelledJob - Response being returned to SB \n" . var_export($response), 'appointments_api_');

        return($response);
    }

}

?>
