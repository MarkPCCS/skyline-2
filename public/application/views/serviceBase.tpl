{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = "Service Appraisal"}
    {$PageId = $HomePage}
{/block}


{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" media="screen" charset="utf-8" />
{/block}


{block name=scripts}
    
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>

<script type="text/javascript" src="{$_subdomain}/js/TableTools.min.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/datatables.api.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/jquery_plugins/jquery.textareaCounter.plugin.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/jquery_plugins/autoresize.jquery.js"></script>
<script type="text/javascript" src="{$_subdomain}/js/jquery_plugins/jquery.autosize-min.js"></script>

<script type="text/javascript">   

    var partsTable;
    var print = false;
    var currentAction;

    $(document).ready(function() {
    
	$("#conditionCode").combobox();
	
	$("#symptomType, #symptomCondition").combobox({
	    change: function(event, ui) {
		$.post(
		    "{$_subdomain}/Job/getSymptomCodes", 
		    { 
			jobID:		    {$JobID}, 
			symptomConditionID: $("#symptomCondition").val(),
			symptomTypeID:	    $("#symptomType").val()
		    }, 
		    function(response) {
			var data = JSON.parse(response);
			var html = "";
			for(var i = 0; i < data.length; i++) {
			    html += "<option value='" + data[i].SymptomCodeID + "' " + (data[i].SymptomCodeID == "{$SymptomCodeID}" ? "selected='selected'" : "") + ">" + data[i].SymptomCode + " &nbsp; " + data[i].SymptomCodeDescription + "</option>";
			}
			$("#symptomCode").html(html);
			$("#symptomCode").next("span").find("input").val("");
		    }
		);
	    }
	});
	
	/*
	$("#symptomCondition").combobox({
	    change: function(event, ui) {
		
	    }
	});
	*/
	
	$("#symptomCode").combobox();
	
	$("#jobType").combobox({
	    create: function(event, ui) {
		$(this).next("span").find("input").css("width", "150px");
	    }
	});
	
	$("#warrantyServiceType").combobox();
	
	$("#warrantyDefectType").combobox();

	$("#completionStatus").combobox({
	    change: function(event, ui) {
		for(var i = 0; i < completionStatuses.length; i++) {
		    if($("#completionStatus").val() == completionStatuses[i].CompletionStatusID) {
			$("#explanation").text(completionStatuses[i].Description);
		    }
		}
	    },
	    create: function(event, ui) {
		$(this).next("span").find("input").attr("name", "completionStatusInput");
	    }
	});


	$("textarea").autosize();

	$("select[name=Manufacturer], select[name=ServiceType], select[name=ProductType], select[name=InboundFault]").combobox();


        function openPrintModal() {

            var html = "<fieldset>\
                            <legend align='center'>Print Options</legend>\
                            <br/>\
                            <input name='printRadio' type='radio' value ='2' /> Print Customer Receipt\
                            <br/>\
                            <input name='printRadio' type='radio' value ='1' /> Print Job Card\
                            <br/>\
                            <input name='printRadio' type='radio' value ='3' /> Print Service Report\
			    <br/>\<br/>\
                            <div style='width:100%; text-align:center;'>\
                                <a href='#' id='print' style='width:50px; display:inline-block;' class='btnConfirm'>Print</a>&nbsp;\
                                <a href='#' id='cancel' style='width:50px; display:inline-block;' class='btnCancel'>Cancel</a>\
                            </div>\
                        </fieldset>\
                       ";

            $.colorbox({
                html :	html, 
                width:	"320px",
                scrolling:	false
            });

        }


	$(document).on("change", "input[name=ServiceAction]", function() {
	    if($("input[name=ServiceAction]:checked").val() == "RepairRequired") {
		var html = "<div id='consignmentWrapper' style='margin-left:25px;'>\
				<label for='ConsignmentNo' style='margin-right:5px;'>Consignment No:</label>\
				<input type='text' value='' name='ConsignmentNo' />\
				<label for='ConsignmentDate' style='margin-left:10px; margin-right:5px;'>Date:</label>\
				<input type='text' value='' name='ConsignmentDate' style='width:75px;' />\
			    </div>\
			   ";
		$(html).insertAfter($("#RepairRequiredLabel"));
		$("input[name=ConsignmentDate]").datepicker({ dateFormat: 'dd/mm/yy' });
	    } else {
		$("#consignmentWrapper").remove();
	    }
	    if($("input[name=ServiceAction]:checked").val() == "BranchRepair") {
		$("#branchRepairForm").show();
		$("#jobFaultCodesWrapper .show").show();
	    } else {
		$("#branchRepairForm").hide();
	    }
	});


        $(document).on("click", "#print", function() {
            var printType = $("input[name=printRadio]:radio:checked").val();
            $.colorbox.close();
            var url = "{$_subdomain}/Job/printJobRecord/{$JobID}/" + printType + "/?pdf=1";
            window.open(url, "_blank");
            window.focus();
	    location.href = "{$_subdomain}/index";
            return false;
        });


        $(document).on("click", "#printModal", function() {
            openPrintModal();
            return false;
        });


	$(document).on("change", "input[name=ServiceAction]", function() {
	    
	    if(currentAction) {
		$("#ServiceReport textarea").val($("#ServiceReport textarea").val().replace(currentAction, ""));
	    }
    
	    var date = new Date();
	    var d = ("0" + date.getDate()).slice(-2) + "/" + ("0" + date.getMonth()).slice(-2) + "/" + date.getFullYear() + " (" + ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2)+")";
	    if($("#ServiceReport textarea").val() != "") {
		$("#ServiceReport textarea").val(($("#ServiceReport textarea").val() + "\n" + $(this).next("label[for=ServiceAction]").text() + " " + d).toUpperCase());
		currentAction = ("\n" + $(this).next("label[for=ServiceAction]").text() + " " + d).toUpperCase();
	    } else {
		$("#ServiceReport textarea").val(($(this).next("label[for=ServiceAction]").text() + " " + d).toUpperCase());
		currentAction = ($(this).next("label[for=ServiceAction]").text() + " " + d).toUpperCase();
	    }
	    
	    $("#ServiceReport textarea").trigger("autosize");
	    $("#ServiceReport textarea").scrollTop($("#ServiceReport textarea")[0].scrollHeight - $("#ServiceReport textarea").height());
	    $("html, body").animate({ scrollTop: $(document).height() }, 1000);
	});
	
    
	

	$(document).on("click", "#cancel", function() {
	    $.colorbox.close();
	    return false;
	});




        $(".inline-edit").click(function(){
            
            var fieldset = $(this).attr('href' );
                
            switch(fieldset.substr(1)){
                case'InvoiceCosts':
                    $(fieldset + ' #discount-type').show();
                    break;
                }
                                
            $(fieldset + ' .inline-save, ' + fieldset + ' .inline-cancel, ' + fieldset + ' .ui-datepicker-trigger, ' + fieldset + ' .fieldInput,' + fieldset + ' .memoInput').show();
               
            $('.inline-edit, ' + fieldset + ' .fieldValue,' + fieldset + ' .memoValue').hide();
            
            if(fieldset=="#InvoiceCosts")   
            {
                 $('#Gross').hide();
                 $('#VAT').hide();
                 $('#TotalNet').hide();
            }
            
                
	    
	    if(fieldset == "#IssueReport") {
		var textCounter = {  
		    maxCharacterSize:	2000,  
		    originalStyle:	"jobUpdateTextCount",  
		    warningStyle:	"warningDisplayInfo",  
		    warningNumber:	1700,  
		    displayFormat:	"#input Characters | #left Characters Left | #words Words"
		};
		$("textarea[name=IssueReport]").textareaCount(textCounter, function() {
		    if($("textarea[name=IssueReport]").val().length > 300) {
			$("textarea[name=IssueReport]").css("color", "red");
		    } else {
			$("textarea[name=IssueReport]").css("color", "inherit");
		    }
		});  
		$(".jobUpdateTextCount").css("font-size", "11px");
	    }
	    

            resizeColumns();
            
            return false;
        });


        $(".inline-cancel").click(function(){
	    location.reload();
            
            var fieldset = $(this).attr('href' );
	    
	    if(fieldset == "#IssueReport") {
		$(".jobUpdateTextCount").remove();
	    }
                
            switch(fieldset.substr(1)) {
                case 'InvoiceCosts':
                    $(fieldset + ' #discount-type').hide();
                    break;
            }                             
                                
            $('.error-highlight').removeClass('error-highlight');
                
            $( fieldset + ' .inline-save, ' + fieldset + ' .inline-cancel, ' + 
                    fieldset + ' .ui-datepicker-trigger, ' + fieldset + ' .fieldInput, ' + fieldset + ' .memoInput, ' + fieldset + ' .error').hide();
                
            $('.inline-edit, ' + fieldset + ' .fieldValue,' + fieldset + ' .memoValue').show(); 
                
            resizeColumns();
            
            if(fieldset=="#InvoiceCosts")
            {
               location.reload();
            }

        });
            
	    
        $('input').change(function(){
            $('.error').hide();
            $('.error-highlight').removeClass( 'error-highlight' );                
        });
            
	    
        $(".inline-save").click(function() {           
	
            var fieldset = $(this).attr('href'); 
                
	    if(fieldset == "#IssueReport") {
		$(".jobUpdateTextCount").remove();
	    }
		
            /*switch(fieldset.substr(1)) {
                case 'InvoiceCosts': {
                    $(fieldset + ' .discount-type').hide();
                    break;
		}
            } */
                
            $(fieldset).validate({
                errorPlacement: function(error, element) {
                    error.insertAfter(element);
                }            
            });
                
            $('html, body').css('cursor', 'wait');
            $( fieldset + ' .inline-save, ' + fieldset + ' .inline-cancel ').attr('disabled', true);
	    
	    console.log("{$_subdomain}/Data/update" + fieldset.substr(1) + '/{$JobID}/{$ClientID}');
	    
            $.post("{$_subdomain}/Data/update" + fieldset.substr(1) + '/{$JobID}/{$ClientID}', 
		    $(fieldset).serializeArray(),
		    function(data) { 
		    
			$('html, body').css('cursor', 'auto');
			$( fieldset + ' .inline-save, ' + fieldset + ' .inline-cancel ').removeAttr('disabled');
			if(data !== null) {
			    var server = jQuery.parseJSON(data);

			    if(server.status == 'error') {

				$(fieldset + ' .error').show();
				$(fieldset + ' .error').text(server.message);
				if (server.field != '') {
				    $(fieldset + ' [name="' + server.field +'"]').addClass('error-highlight');
				} 
                                resizeColumns();
			    } else {   

                            if ('ModelNumber' in server.data) $( 'ModelNumber' ).text( server.data['ModelNumber'] );
                            if ('UnitTypeID' in server.data) $( 'UnitTypeID' ).text( server.data['UnitTypeID']);

				$(fieldset + ' .memoInput').each(function() {
				    elt = $(this);                               
				    elt.parent().children('.memoValue').text(elt.children('textarea').val());
				});

				$(fieldset + ' .fieldInput').each(function() {

				    elt = $(this);

				    if(elt.has("textarea").length) {
					if($(this).children('textarea').attr('id') == 'ModelDescription') {
					    if(server.data['ModelDescription'] == '' || server.data['ModelDescription'] == null) {
						$(this).parent().children('.fieldValue').text(server.data['ModelNumber']);
						$(this).children('textarea').val(server.data['ModelNumber']);                                       
					    } else {
						$(this).parent().children('.fieldValue').text(server.data['ModelDescription']);
						$(this).children('textarea').val(server.data['ModelDescription']);
					    }
					} else {
					    $(this).parent().children('.fieldValue').text($(this).children('textarea').val());
					}
				    } else if(elt.has("select").length) {
					//alert('select: '+elt.find('option:selected').text());
					if ($(this).children('select').attr('id') == 'ManufacturerID') {
					    $(this).parent().children('.fieldValue').text(server.data['ManufacturerName']);
					    $("#ServiceBaseManufacturer").val(server.data['ManufacturerName']);
					    //$('#ManufacturerID option[value="'+server.data['ManufacturerID']+'"').attr('selected','selected');
					} else if($(this).children('select').attr('id') == 'ProductTypeID') {
					    $(this).parent().children('.fieldValue').text(server.data['UnitTypeName']);
					    $("#ServiceBaseUnitType").val(server.data['UnitTypeName']);
					} else {
					    $(this).parent().children('.fieldValue').text($(this).find('option:selected').text());
					}
				    } else if(elt.has("input").length) {
					$(this).parent().children('.fieldValue').text($(this).children('input').val());
				    } 

				});

				$('.error-highlight').removeClass('error-highlight');

				$(fieldset + ' .inline-save, ' + fieldset + ' .inline-cancel, ' + 
				  fieldset + ' .ui-datepicker-trigger, ' + fieldset + ' .fieldInput, ' + 
				  fieldset + ' .memoInput, ' + fieldset + ' .error').hide();

				$('.inline-edit, ' + fieldset + ' .fieldValue,' + fieldset + ' .memoValue').show(); 

				resizeColumns();

			    }
			}

                         if(fieldset=="#InvoiceCosts" && server.status != 'error')
                         {
                            location.reload();
                         }

		    }
		    
	    );

	    location.reload();
            return false;
	    
        });




        $('.head').click(function() {
	    var the = this;
            $(this).children('.icon_c').toggleClass('icon_e');
            $('html, body').animate({ scrollTop: $(the).offset().top }, 800);
            $(this).next().slideToggle('slow');
            return false;
        }).next().hide();


	$(document).on("click", "#mainComplete, #printModal", function() {
	
	    if($(this).attr("id") == "printModal") {
		print = true;
	    }
	
	    if($("input[name=ServiceAction]").is(":checked") && !$("#RepairRequired").is(":checked") && !$("#BranchRepair").is(":checked")) {
	    
		var html = "<fieldset id='endAppraisal' style='position:relative;'>\
				<legend align='center'>Appraisal & Job Complete</legend>\
				<p style='margin:5px 0px;'>This Job has been Completed. What section would you like to move it to?</p>\
				<div style='width:100%; text-align:center'>\
				    <form id='endForm' style='display:inline-block; text-align:left;'>\
					<input type='radio' name='endOptions' value='service_complete' /> Service Complete <br/>\
					<input type='radio' name='endOptions' value='customer_notified' /> Customer Notified <br/>\
					<input type='radio' name='endOptions' value='returned_to_customer' /> Returned to Customer <br/>\
				    </form>\
				</div>\
				<div style='text-align:center; margin-top:10px;'>\
				    <a href='#' id='endComplete' class='btnConfirm'>Save</a>\
				    <a href='#' id='endCancel' class='btnCancel'>Cancel</a>\
				</div>\
			    </fieldset>\
			   ";
    
		$.colorbox({
		    width:	    "400px",
		    html:	    html, 
		    scrolling:  false
		});

	    } else if($("#RepairRequired").is(":checked")) {
	    
		var html = "<fieldset id='endAppraisal' style='position:relative;'>\
				<legend align='center'>3rd Party Repair</legend>\
				<p style='margin:5px 0px;'>\
				    This job has not been completed. It has been allocated for external repair. What section would you \
				    like to move it too?\
				</p>\
				<div style='width:100%; text-align:center'>\
				    <form id='endForm' style='display:inline-block; text-align:left;'>\
					<input type='radio' name='endOptions' value='awaiting_collection' /> Awaiting Collection <br/>\
					<input type='radio' name='endOptions' value='with_service_provider' /> Job with Service Provider <br/>\
				    </form>\
				</div>\
				<div style='text-align:center; margin-top:10px;'>\
				    <a href='#' id='endComplete' class='btnConfirm'>Save</a>\
				    <a href='#' id='endCancel' class='btnCancel'>Cancel</a>\
				</div>\
			    </fieldset>\
			   ";
    
		$.colorbox({
		    width:	    "400px",
		    html:	    html, 
		    scrolling:  false
		});

	    } else if($("#BranchRepair").is(":checked")) {
	
		$("#branchRepairForm").validate = null;

		$("#branchRepairForm").validate({
		    rules: {
			conditionCode:		{ required: true},
			symptomCode:		{ required: true},
			warrantyServiceType:	{ required: true},
			warrantyDefectType:	{ required: true},
			jobCategory:		{ required: true},
			serviceCategory:	{ 
						    required: function() { 
								  if($("input[name=jobCategory]:checked").val() == "service") {
								      return true;
								  } else {
								      return false;
								  }
							      } 
						},
			completionStatusInput:  { required: true }
		    },
		    messages: {
			conditionCode:		{ required: "Condition Code is required" },
			symptomCode:		{ required: "Symptom Code is required" },
			warrantyServiceType:	{ required: "Service Type is required" },
			warrantyDefectType:	{ required: "Defect Type is required" },
			jobCategory:		{ required: "Job Category is required" },
			serviceCategory:	{ required: "Service Type is required" },
			completionStatusInput:  { required: "Completion Status is required" }
		    },
		    errorPlacement: function(error, element) {
			switch (element.attr("name")) {
			    case "conditionCode":
			    case "symptomCode":
			    case "warrantyServiceType":
			    case "warrantyDefectType": {
				error.insertAfter("#faultCodesDiv");
				break;
			    }
			    case "jobCategory": {
				error.css("padding-left", "140px");
				error.insertAfter("#jobCategoryDiv");
				break;
			    }
			    case "serviceCategory": {
				error.css("padding-left", "140px");
				error.insertAfter("#serviceCategoryDiv");
				break;
			    }
			    case "completionStatusInput": {
				error.css("padding-left", "140px");
				error.insertAfter($("#completionStatus").next(".ui-combobox"));
				break;
			    }
			    default: {
				error.insertAfter(element);
				break;
			    }
			}
		    },
		    ignore:	    "",
		    errorClass:	    "fieldError",
		    onkeyup:	    false,
		    onblur:	    false,
		    errorElement:   "p",
		    submitHandler: function() {
		    
			if(!$("#declaration").is(":checked")) {
			    alert("You must accept the declaration in order to complete the job.");
			    return false;
			}
			
			var html = "<fieldset id='endAppraisal' style='position:relative;'>\
					<legend align='center'>Appraisal & Job Complete</legend>\
					<p style='margin:5px 0px;'>This Job has been Completed. What section would you like to move it to?</p>\
					<div style='width:100%; text-align:center'>\
					    <form id='endForm' style='display:inline-block; text-align:left;'>\
						<input type='radio' name='endOptions' value='service_complete' /> Service Complete <br/>\
						<input type='radio' name='endOptions' value='customer_notified' /> Customer Notified <br/>\
						<input type='radio' name='endOptions' value='returned_to_customer' /> Returned to Customer <br/>\
					    </form>\
					</div>\
					<div style='text-align:center; margin-top:10px;'>\
					    <a href='#' id='endComplete' class='btnConfirm'>Save</a>\
					    <a href='#' id='endCancel' class='btnCancel'>Cancel</a>\
					</div>\
				    </fieldset>\
				   ";

			$.colorbox({
			    width:	    "400px",
			    html:	    html, 
			    scrolling:  false
			});
			
			return false;
		    }
		});

		$("#branchRepairForm").submit();

	    }
		
	    return false;
	    
	});


	$(document).on("click", "#mainSave", function() {

	    if($("input[name=ServiceAction]").is(":checked") && $("#BranchRepair").is(":checked")) {

		var html = "<fieldset id='endAppraisal' style='position:relative;'>\
				<legend align='center'>Save Appraisal</legend>\
				<p style='margin:5px 0px;'>Any changes you have made will be saved and this job will be moved to the <strong>Branch Repair</strong> section.</p>\
				<div style='text-align:center; margin-top:10px;'>\
				    <a href='#' id='endSave' class='btnConfirm'>Save</a>\
				    <a href='#' id='endCancel' class='btnCancel'>Cancel</a>\
				</div>\
			    </fieldset>\
			   ";
	    
	    } else {
	    
		var html = "<fieldset id='endAppraisal' style='position:relative;'>\
				<legend align='center'>Save Appraisal</legend>\
				<p style='margin:5px 0px;'>Any changes you have made will be saved, but this job will remain in the <strong>Appraisal Required</strong> section.</p>\
				<div style='text-align:center; margin-top:10px;'>\
				    <a href='#' id='endSave' class='btnConfirm'>Save</a>\
				    <a href='#' id='endCancel' class='btnCancel'>Cancel</a>\
				</div>\
			    </fieldset>\
			   ";
	    
	    }
	    
	    $.colorbox({
		width:	    "400px",
		html:	    html, 
		scrolling:  false
	    });
	    
	    return false;
	});


	$(document).on("click", "#endSave", function() {
	    
	    var data =	$("#dropdownForm").serialize() + "&" + 
			$("#textareaForm").serialize() + "&" +
			$("#actionForm").serialize() + "&" +
			$("#endForm").serialize() + "&" +
			$("#branchRepairForm").serialize();
	    
	    $.ajax({
		type:	"POST",
		url:	"{$_subdomain}/Job/endAppraisal",
		data:	data,
		cache:	false,
		success: function(response) {
		    location.assign("{$_subdomain}/index");
		}
	    });
	    
	    return false;
	});


	$(document).on("click", "#endComplete", function() {
	    
	    if(($("#SoftwareUpdate").is(":checked") || 
		$("#CustomerEducation").is(":checked") || 
		$("#SettingsSetup").is(":checked") || 
		$("#RepairRequired").is(":checked") ||
		$("#DamagedBeyondRepair").is(":checked") ||
		$("#FaultyAccessory").is(":checked") ||
		$("#NonUKModel").is(":checked") ||
		$("#Unresolvable").is(":checked")
	       ) && !$("input[name=endOptions]").is(":checked")) {
	
		alert("You must select one of the options.");
		return false;
	    }
	    
	    var data =	$("#dropdownForm").serialize() + "&" + 
			$("#textareaForm").serialize() + "&" +
			$("#actionForm").serialize() + "&" +
			$("#endForm").serialize() + "&" +
			$("#branchRepairForm").serialize();
	    
	    data += "&complete=1";
	    
	    $.ajax({
		type:	"POST",
		url:	"{$_subdomain}/Job/endAppraisal",
		data:	data,
		cache:	false,
		success: function(response) {
		    if(print) {
			openPrintModal();
			print = false;
		    } else {
			location.assign("{$_subdomain}/index");
		    }
		}
	    });
	    
	    return false;
	});


	$(document).on("click", "#endCancel", function() {
	    $.colorbox.close();
	    return false;
	});
	
	
	$(document).on("click", ".btnCancel", function() {
	    $.colorbox.close();
	    return false;
	});
	
	
	$(document).on("click", "#saveQAQuestions", function () {
	    var data = [];
	    $("#qaQuestionList input").each(function() {
		data.push({ id: $(this).attr("questionID"), text: $(this).val() });
	    });
	    var json = JSON.stringify(data);
	    $.post("{$_subdomain}/Job/updateQAQuestions", { data: json }, function(response) {
		$.colorbox.close();
		$.post("{$_subdomain}/Job/getQAQuestions", { }, function(response) {
		    var data = $.parseJSON(response);
		    var html = "";
		    for(var i = 0; i < data.length; i++) {
			html += "   <div style='width:309px; position:relative; float:left;'>\
					<input name='qaQuestions[]' type='checkbox' value='" + data[i].QAQuestionID + "' /> " + data[i].QuestionText +
				    "</div>";
		    }
		    $("#qaQuestions").html(html);
		});
	    });
	    return false;
	});


	$(document).on("click", "#addQAQuestion", function () {
	    var html = "<input type='text' style='width:95%' questionID='' value='' /><br/>";
	    $(html).appendTo("#qaQuestionList");
	    $.colorbox.resize();
	    return false;
	});
	
	
	$(document).on("click", "#qaSetup", function() {
	    $.post("{$_subdomain}/Job/getQAQuestions", { }, function(response) {
		var data = $.parseJSON(response);
		var questions = "<div id='qaQuestionList'>";
		for(var i = 0; i < data.length; i++) {
		    questions += "<input type='text' style='width:95%;' questionID='" + data[i].QAQuestionID + "' value='" + data[i].QuestionText + "' /><br/>";
		}
		questions += "</div>";
		var html = "<fieldset style='padding-top:10px; text-align:center;'>\
				<legend align='center'>Edit QA Questions</legend>\
				" + questions + "\
				<br/>\
				<a href='#' class='btnStandard' id='addQAQuestion'>Add</a>\
				\
				<br/><br/>\
				<a href='#' class='btnConfirm' id='saveQAQuestions' style='margin-right:5px;'>Save</a>\
				<a href='#' class='btnCancel'>Cancel</a>\
			    </fieldset>\
			   ";
		$.colorbox({
		    width:	"400px",
		    html:	html, 
		    scrolling:  false
		});
	    });
	    return false;
	});
	
	
	$(document).on("change", "input[name=jobCategory]", function() {
	    $("#completionStatus").next(".ui-combobox").find(".ui-combobox-input").val("");
	    $("#explanation").text("");
	    $("#completionStatus").html("");
	    if($(this).val() == "service") {
		$("#serviceCategoryDiv").show();
	    } else {
		$("input[name=serviceCategory]").prop("checked", false);
		$("#serviceCategoryDiv").hide();
		populateCompletionStatus($(this).val(), null);
	    }
	});


	$(document).on("change", "input[name=serviceCategory]", function() {
	    $("#explanation").text("");
	    $("#completionStatus").next(".ui-combobox").find(".ui-combobox-input").val("");
	    populateCompletionStatus("service", $(this).val());
	    $("#jobCategory").next(".ui-combobox-input").val("");
	});


	function populateCompletionStatus(jobCategory, serviceCategory) {
	    $("#completionStatus").html("");
	    $.post("{$_subdomain}/Job/getCompletionStatuses", { jobCategory: jobCategory, serviceCategory: serviceCategory }, function(response) {
		var data = $.parseJSON(response);
		completionStatuses = data;
		var html = "";
		for(var i = 0; i < data.length; i++) {
		    html += "<option value='" + data[i].CompletionStatusID + "'>" + data[i].CompletionStatus + "</option>";
		}
		$("#completionStatus").html(html);
	    });
	}


	$(document).on("click", "#cancelAppraisal", function() {
	    location.href = "{$_subdomain}/index/index/?jobsTable=" + getUrlVars()["ref"];
	});


	if("{$serviceAction}" == "BranchRepair") {
	    $("#jobFaultCodesWrapper .show").show();
	}
	


    });



</script>
<style>
.largeButton{
    width:150px !important;
    }
.hidden{ display:none !important;}
</style>

{/block}


{block name=body}

<div class="breadcrumb">
    <div>
	{if isset($ref) && $ref != ""}
            {if $ref == "inStore" || $ref == "withSupplier" || $ref == "awaitingCollection" || $ref == "customerNotified" || $ref == "authRequired" || $ref == "responseRequired" || $ref == "appraisalRequired"}
                <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/index/index?jobsTable={$ref}">Open Jobs</a> / &nbsp;Job Update
            {else if $ref == "raJobs"}
		{if isset($params)}
		    <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/raJobs/{$params}">RA Jobs</a> / &nbsp;Job Update
		{else}
		    <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/raJobs">RA Jobs</a> / &nbsp;Job Update
		{/if}
	    {else if $ref == "openJobs"}
                <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/openJobs">Open Jobs</a> / &nbsp;Job Update
	    {else if $ref == "overdueJobs"}
                <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/Job/overdueJobs">Overdue Jobs</a> / &nbsp;Job Update
	    {else if $ref == "jobSearch"}
		<a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/index/jobsearch{$searchArgs}">Job Search</a> / &nbsp;Job Update
	    {else}
                <a href="{$_subdomain}/index/index">Home Page</a> / <a href="{$_subdomain}/index/index?jobsTable={$ref}">Overdue Jobs</a> / &nbsp;Job Update
            {/if}
	{else}
	    <a href="{$_subdomain}/index/index">Home Page</a> / &nbsp;Job Update
	{/if}
    </div>
</div>
    
<div id="sbWrapper">

    <div id="sbHeader" style="text-align:center;">
	<img src="{$_subdomain}/images/service_base.png" style="display:inline-block; position:relative; float:left;"/>
	<span style="margin:30px 0px 0px 0px; display:inline-block; font-size: 15px;">
	    SL No: {$JobID} - {$ContactLastName}, {$ContactTitle} {$ContactFirstName}
	</span>
	<span style="margin:30px 0px 0px 0px; display:block; position:relative; float:right;">
	    Date Booked: {$DateBooked|date_format: "%d/%m/%Y"} {$TimeBooked|date_format: "(%H:%M)"}
	</span>
    </div>
	
    <div id="sbDropdowns">

	<form id="dropdownForm">
	
	    <div id="sbDropdownsLeft">

		<p>
		    <label for="ModelNo">{$page["Labels"]["model_no"]}:</label>
		    <input type="text" value="{$ModelNumber|default:$service_base_model|upper|escape:'html'}" name="ModelNo" />
		</p>

		<p>
		    <label for="Manufacturer">Manufacturer:</label>
		    <select name="Manufacturer">
			<option value=""></option>
			{foreach $manufacturerList as $man}
			    <option value="{$man.ManufacturerID}" {if $man.ManufacturerID == $ProductDetails.ManufacturerID}selected="selected"{/if}>{$man.ManufacturerName|escape:'html'}</option>
			{/foreach}
		    </select>
		</p>

		<p>
		    <label for="ProductType">Product Type:</label>
		    <select name="ProductType">
			<option value=""></option>
			{foreach $productTypeList as $productType}
			    <option value="{$productType.UnitTypeID}" {if $productType.UnitTypeID == $UnitTypeID || $productType.UnitTypeName == $service_base_unit_type}selected="selected"{/if}>{$productType.UnitTypeName|escape:'html'}</option>
			{/foreach}
		    </select>
		</p>

		<p>
		    <label for="SerialNo">Serial No:</label>
		    <input type="text" value="{$SerialNo|escape:'html'}" name="SerialNo" />
		</p>

		<p>
		    <label for="SerialNo">IMEI No:</label>
		    <input type="text" value="{$ImeiNo|escape:'html'}" name="ImeiNo" />
		</p>

	    </div>

	    <div id="sbDropdownsRight">

		<p>
		    <label for="ServiceType">Service Type:</label>
		    <select name="ServiceType">
			<option value=""></option>
			{foreach $serviceTypes as $stype}
			    <option value="{$stype.ServiceTypeID}" {if $ProductDetails.ServiceTypeName|trim|strtoupper == $stype.Name|strtoupper}selected="selected"{/if}>{$stype.Name|escape:'html'}</option>
			{/foreach}
		    </select>
		</p>

		<p style="margin-top:15px;">
		    <label>DOP: </label> {$DateOfPurchase|date_format: "%d/%m/%Y"}
		</p>
		
		<p style="margin-top:15px;">
		    <label>Authorisation No: </label> {$AdditionalInformation.AuthorisationNo}
		</p>
		
		<p style="margin-top:15px;">
		    <label for="InboundFault">Inbound Fault: </label>
		    <select name="InboundFault">
			<option value=""></option>
			{foreach $inboundFaults as $fault}
			    <option value="{$fault.InboundFaultCodeID}" {if $InboundFaultCodeID == $fault.InboundFaultCodeID}selected="selected"{/if}>{$fault.Code|escape:'html'}</option>
			{/foreach}
		    </select>
		</p>

	    </div>

	</form>
	    
    </div>
 
    {if $ProductLocation eq 'BRANCH' || $ProductLocation eq 'SERVICE PROVIDER'}                
    
        <div id="sbAccessories">
            <span>ACCESSORIES: </span> {$Accessories}
        </div>

        <div id="sbCondition">
            <span>CONDITION: </span> {$UnitCondition}
        </div>
    
    {/if}

    {if $Timeline|@count gt 0}
	<div id="timeline_container" class="clearfix">
	    <div class="standardTAT">
		<div class="left">{$DaysFromBooking} Day(s) From Booking</div>
		<div class="centre">Standard Job Turnaround Time = {$JobTurnaroundTime} Day(s)</div>
		<div class="right">
		    {if $DaysRemaining >= 0}
			{$DaysRemaining} Day(s) Remaining
		    {else}
			{math equation = "x * y" x = -1 y = $DaysRemaining} Day(s) Overdue
		    {/if}
		</div>
	    </div>
	    <div>
		<div class="left turnaround-timeline{$TurnaroundTimeType}">
		    <div class="value" style="width:{$TurnaroundTimeValue};">&nbsp;</div>
		</div>
	    </div>  
	</div>           
    {/if}        

    <form id="textareaForm">
    
	<div id="ServiceRequest">
	    <p>Service Request</p>
	    <textarea name="ServiceRequest">{$ReportedFault|escape:'html'}</textarea>
	</div>

	<div id="ServiceReport">
	    <p>Service Report</p>
	    <textarea name="ServiceReport">{$RepairDescription}</textarea>
	</div>
	
    </form>

	
    <div id="sbBottomSection">

	<fieldset id="sbServiceAction">
	    <legend>Service Action</legend>

	    <form id="actionForm">
	    
		<div id="softService">
		    
		    <div id="actionLeft" style="width:50%; float:left; position:relative; display:block; margin-top:10px;">

			<h3 style="margin-bottom:0; margin-left:25px;">Soft Service</h3>

			<input type="radio" name="ServiceAction" value="SoftwareUpdate" id="SoftwareUpdate" {if $serviceAction == "SoftwareUpdate"}checked="checked"{/if} style="margin-top:10px;" />
			<label for="ServiceAction">Software Update</label>
			<br/>

			<input type="radio" name="ServiceAction" value="CustomerEducation" id="CustomerEducation" {if $serviceAction == "CustomerEducation"}checked="checked"{/if} style="margin-top:5px;" />
			<label for="ServiceAction">Customer Education/Tuition</label>
			<br/>

			<input type="radio" name="ServiceAction" value="SettingsSetup" id="SettingsSetup" {if $serviceAction == "Settings"}checked="checked"{/if} style="margin-top:5px;" />
			<label for="ServiceAction">Settings/Setup</label>
			<br/>

		    </div>

		    <div id="actionRight" style="width:50%; float:left; position:relative; display:block; margin:10px 0px;">

			<h3 style="margin-bottom:0; margin-left:25px;">Technical Issue</h3>

			<input type="radio" name="ServiceAction" value="DamagedBeyondRepair" id="DamagedBeyondRepair" {if $serviceAction == "DamagedBeyondRepair"}checked="checked"{/if} style="margin-top:10px;" />
			<label for="ServiceAction">Damaged Beyond Repair</label>
			<br/>

			<input type="radio" name="ServiceAction" value="FaultyAccessory" id="FaultyAccessory" {if $serviceAction == "FaultyAccessory"}checked="checked"{/if} style="margin-top:5px;" />
			<label for="ServiceAction">Faulty Accessory</label>
			<br/>

			<input type="radio" name="ServiceAction" value="NonUKModel" id="NonUKModel" {if $serviceAction == "NonUKModel"}checked="checked"{/if} style="margin-top:5px;" />
			<label for="ServiceAction">Non-UK Model</label>
			<br/>

			<input type="radio" name="ServiceAction" value="Unresolvable" id="Unresolvable" {if $serviceAction == "Unresolvable"}checked="checked"{/if} style="margin-top:5px;" />
			<label for="ServiceAction">Un-resolvable</label>
			<br/>

		    </div>
		
		</div>    
		    
		<hr style="margin:10px 0px;" />

		<h3 style="margin-bottom:0; margin-left:25px; margin-bottom:5px;">Repair Required</h3>
		
		<input type="radio" name="ServiceAction" value="BranchRepair" id="BranchRepair" {if $serviceAction == "BranchRepair"}checked="checked"{/if} style="margin-top:5px;" />
		<label for="ServiceAction" id="BranchRepairLabel">Branch Repair</label>
		<br/>
		
		<input type="radio" name="ServiceAction" value="RepairRequired" id="RepairRequired" {if $serviceAction == "RepairRequired"}checked="checked"{/if} style="margin-top:5px;" />
		<label for="ServiceAction" id="RepairRequiredLabel">3rd Party Repair</label>
		<br/>
		
		<input type="hidden" name="sbJobID" value="{$JobID}" />
	    
	    </form>

	</fieldset>

	<fieldset id="sbInvoiceCosts">
	    <legend align="center">Invoice Costs</legend>


	    <form id="InvoiceCosts">
		<div id="InvoiceCostsPanel">  
		    {if $AP7024}
			{if $ServiceTypeCode == 2}
			    <div style="height: 100%;">
				<p>
				    This Job has been booked as a Manufacturer's Warranty claim, please check all information is correct before proceeding with this job.
				</p>
			    </div>
			{else}
			    <div style="height: 100%;">
				<p class="error" style="display:none;"></p>
				<p id="Parts">
				    <label>Parts:</label>
				    <span class="fieldValue">{$InvoiceCosts.Parts}</span>    
				    <span class="fieldInput"> <input type="text" class="" name="Parts" value="{$InvoiceCosts.Parts}" /></span>                    
				</p> 
				<p id="Labour">
				    <label>Labour:</label>
				    <span class="fieldValue">{$InvoiceCosts.Labour}</span>   
				    <span class="fieldInput"> <input type="text" class="" name="Labour" value="{$InvoiceCosts.Labour}" /></span>                     
				</p> 
				<p id="Carriage">
				    <label>Carriage:</label>
				    <span class="fieldValue">{$InvoiceCosts.Carriage}</span>    
				    <span class="fieldInput"> <input type="text" class="" name="Carriage" value="{$InvoiceCosts.Carriage}" /></span>                    
				</p> 
				<p id="TotalNet">
				    <label>Total Net:</label>
				    <span class="fieldValue">{$InvoiceCosts.TotalNet}</span>   
				    <span class="fieldInput"> <input type="text" class="" name="TotalNet" value="{$InvoiceCosts.TotalNet}" /></span>                     
				</p> 
				<p id="VAT">
				    <label>VAT @ {$InvoiceCosts.VATRate}%:</label>
				    <span class="fieldValue">{$InvoiceCosts.VAT}</span>    
				    <span class="fieldInput"> 
					<input type="text" class="" name="VAT" value="{$InvoiceCosts.VAT}" />     
				    </span>                    
				</p> 
				<p id="Gross">
				    <label>Gross:</label>
				    <span class="fieldValue">{$InvoiceCosts.Gross}</span>   
				    <span class="fieldInput"> <input type="text" class="" name="Gross" value="{$InvoiceCosts.Gross}" /></span>                     
				</p> 
			    </div>
			    <p style="text-align: center; margin-bottom: 0; margin-top:10px;">
				<a class="hide inline-save btnConfirm" style="display:none" title="" href="#InvoiceCosts">Save</a>
				<a class="inline-cancel hide btnCancel" style="display:none" title="" href="#InvoiceCosts">Cancel</a>
				<a class="inline-edit btnStandard" title="" href="#InvoiceCosts">Edit</a>
			    </p>
			</div>
		    {/if}
		{/if}
	    </form>


	</fieldset>

    </div>
	
	
    <form name="branchRepairForm" id="branchRepairForm" style="position:relative; float:left; {if $serviceAction == "BranchRepair"}display:block;{else}display:none;{/if}">
	    
	{include file="include/partsused.tpl"}


	<div id="jobFaultCodesWrapper" style="position:relative; float:left; width:100%;">
	    <div class="head"><div class="icon_c">&nbsp;</div>Job Fault Codes (<span id="faultCodeCount">{$faultCodeCount}</span>)</div>
	    <div class="show" style="margin-top:10px; {if $serviceAction == "BranchRepair"}display:block;{/if}">
		<div id="faultCodesDiv" style="position:relative; float:left;">
		    <div style="position:relative; float:left; margin-right:20px;">
			<label for="conditionCode" style="margin-bottom:5px;">Condition Code<sup>*</sup></label>
			<br/>
			<select id="conditionCode" name="conditionCode" style="width:100px;">
			    <option value="">Select Condition Code</option>
			    {foreach $conditionCodes as $code}
				<option value="{$code.ConditionCodeID}" {if $code.ConditionCodeID == $ConditionCodeID}selected="selected"{/if}>{$code.ConditionCode} &nbsp; {$code.ConditionCodeDescription}</option>
			    {/foreach}
			</select>
		    </div>
		    <div style="position:relative; float:left; margin-right:20px;">
			<label for="symptomType" style="margin-bottom:5px;">Symptom Category</label>
			<br/>
			<select id="symptomType" name="symptomType" style="width:100px;">
			    <option value="">Select Symptom Category</option>
			    {foreach $symptomTypes as $type}
				<option value="{$type.SymptomTypeID}">{$type.SymptomTypeName}</option>
			    {/foreach}
			</select>
		    </div>
		    <div style="position:relative; float:left; margin-right:20px;">
			<label for="symptomCondition" style="margin-bottom:5px;">Symptom Type</label>
			<br/>
			<select id="symptomCondition" name="symptomCondition" style="width:100px;">
			    <option value="">Select Symptom Type</option>
			    {foreach $symptomConditions as $condition}
				<option value="{$condition.SymptomConditionID}">{$condition.SymptomConditionName}</option>
			    {/foreach}
			</select>
		    </div>
		    <div style="position:relative; float:left;">
			<label for="symptomCode" style="margin-bottom:5px;">Symptom Code<sup>*</sup></label>
			<br/>
			<select id="symptomCode" name="symptomCode" style="width:100px;">
			    <option value="">Select Symptom Code</option>
			    {foreach $symptomCodes as $code}
				<option value="{$code.SymptomCodeID}" {if $code.SymptomCodeID == $SymptomCodeID}selected="selected"{/if}>{$code.SymptomCode} &nbsp; {$code.SymptomCodeDescription}</option>
			    {/foreach}
			</select>
		    </div>
		    <div style="position:relative; float:left; margin-right:20px;">
			<label for="warrantyServiceType" style="margin-bottom:5px;">Service Type<sup>*</sup></label>
			<br/>
			<select id="warrantyServiceType" name="warrantyServiceType" style="width:100px;">
			    <option value="">Select Service Type</option>
			    {foreach $warrantyServiceTypes as $type}
				<option value="{$type.WarrantyServiceTypeID}" {if $type.WarrantyServiceTypeID == $WarrantyServiceTypeID}selected="selected"{/if}>{$type.Code} &nbsp; {$type.Description}</option>
			    {/foreach}
			</select>
		    </div>
		    <div style="position:relative; float:left;">
			<label for="warrantyDefectType" style="margin-bottom:5px;">Defect Type<sup>*</sup></label>
			<br/>
			<select id="warrantyDefectType" name="warrantyDefectType" style="width:100px;">
			    <option value="">Select Defect Type</option>
			    {foreach $warrantyDefectTypes as $type}
				<option value="{$type.WarrantyDefectTypeID}" {if $type.WarrantyDefectTypeID == $WarrantyDefectTypeID}selected="selected"{/if}>{$type.Code} &nbsp; {$type.Description}</option>
			    {/foreach}
			</select>
		    </div>
		</div>
	    </div>
	</div>


	<fieldset style="position:relative; float:left; clear:both; width:100%; margin-top:10px;">
	    <legend>QA Check Sheet</legend>
	    <a href="#" id="qaSetup" style="display:block; position:relative; float:right;">QA Setup</a>
	    <div id="qaQuestions" style="clear:both; position:relative; float:left; width:100%; margin-top:10px;">
		{foreach $qaQuestions as $question}
		    <div style="width:309px; position:relative; float:left;">
			<input name="qaQuestions[]" type="checkbox" value="{$question.QAQuestionID}" {if in_array($question.QAQuestionID, $qaQuestionData)}checked="checked"{/if} />
			{$question.QuestionText}
		    </div>
		{/foreach}
	    </div>
	</fieldset>
	<fieldset id="completionStatusFieldset" style="position:relative; float:left; clear:both; width:100%; padding-top:10px; margin-top:10px;">
	    <legend>Completion Status</legend>
	    <div id="jobCategoryDiv">
		<label for="jobCategory" style="width:120px; text-align:left;">Job Type:</label>
		<input type="radio" name="jobCategory" value="service" {if $JobCategory == "SERVICE"}checked="checked"{/if} /> Service
		<input type="radio" name="jobCategory" value="installation" {if $JobCategory == "INSTALLATION"}checked="checked"{/if} /> Installation
		<input type="radio" name="jobCategory" value="in_home_support" {if $JobCategory == "IN_HOME_SUPPORT"}checked="checked"{/if} /> In-home support
	    </div>
	    <div style="margin-top:10px; {if $JobCategory != "SERVICE"}display:none;{/if}" id="serviceCategoryDiv">
		<label for="serviceCategory" style="width:120px; text-align:left;">Service Type:</label>
		<input type="radio" name="serviceCategory" value="physical" {if $serviceCategory == "physical"}checked="checked"{/if} /> Physical Service
		<input type="radio" name="serviceCategory" value="soft" {if $serviceCategory == "soft"}checked="checked"{/if} /> Soft Service
	    </div>
	    <div style="margin-top:10px;">
		<label for="completionStatus" style="width:120px;">Completion Status:</label>
		<select id="completionStatus" class="required" name="completionStatus">
		    <option value="">Select Completion Status</option>
		    {foreach $completionStatuses as $status}
			<option value="{$status.CompletionStatusID}" {if $status.CompletionStatusID == $CompletionStatusID}selected="selected"{/if}>{$status.CompletionStatus}</option>
		    {/foreach}
		</select>
	    </div>
	    <div style="margin-bottom:10px; margin-top:5px; position:relative; float:left; display:block;">
		<label for="explanation" style="width:120px; text-align:left; display:block; position:relative; float:left;">Explanation:</label>
		<span id="explanation" style="color:red; padding-top:0; display:block; position:relative; margin-left:5px; float:left; width:790px;">{$explanation}</span> 
	    </div>
	    <div style="clear:both;">
		<label for="declaration" style="width:120px; text-align:left;">Declaration:</label>
		<input type="checkbox" id="declaration" value="" />
		<span>I, {$loggedin_user->ContactFirstName} {$loggedin_user->ContactLastName}, confirm these details have been thoroughly checked by myself and are correct to the best of my knowledge.</span>
	    </div>
	</fieldset>

    </form>
	    
    <div id="sbActions" style="margin-top:20px;">
	<a href="#" id="mainSave" class="btnConfirm" style="margin-right:10px;">Save</a>
	<a href="#" id="mainComplete" class="btnConfirm" style="margin-right:10px; width:70px;">Complete</a>
	<a href="#" id="printModal" class="btnConfirm" style="margin-right:10px; width:110px;">Complete & Print</a>
	<a href="{$_subdomain}/index/index?jobsTable={$ref}" id="cancelAppraisal" class="btnCancel">Cancel</a>
    </div>
    
</div>
    
   <div id="ViaDiv2" style="display:none;font-size: 14px;width:300px;margin-bottom: 0px;height:150px">
     
         <fieldset>
             <legend>Update Part Status</legend>
             <p style="color:6b6b6b">Please confirm you have fitted the selected part to this job?</p>
             <br>
       
           <button id=""  type=button class="btnStandard"  onclick="fitPart();$.colorbox.close();" style="width:100px;float:left"><span class="label">Yes</span></button>   
           <button id=""  type=button class="btnStandard" onclick="$.colorbox.close()" style="width:100px;float:right"><span class="label">No</span></button>   
          </fieldset>
        </div>  


{/block}
