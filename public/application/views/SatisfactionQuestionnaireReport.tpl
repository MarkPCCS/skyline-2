{extends "DemoLayout.tpl"}
    {block name=config}
    {$Title = $page['Text']['page_title_report']|escape:'html'}     
    {$PageId = $SatisfactionQuestionnaire}
    {/block}
    {block name=afterJqueryUI}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />       
        <style type="text/css" >
        .ui-combobox-input {
            width:300px;
        }        
        </style>
    {/block}
    {block name=scripts}

<script type="text/javascript">

    $(document).ready(function() {
      
    $("#bId").combobox({
            change: function() {
                if($("#bId").val() != "") {
                    $(location).attr('href', '{$_subdomain}/LookupTables/satisfactionQuestionnaire/report/'+urlencode($("#bId").val()));
                }
            }
        });  
    
                //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {
                                  $(location).attr('href', '{$_subdomain}/index/siteMap');
                                });

                     
                //click handle for run reports                     
                  $(document).on('click', '#report_btn', 
                                function() {
                                            list="";  
                                            if($("input:checked").length==0){
                                            alert("Please check Questionnaire");
                                            return false;
                                            }
                                            else
                                                {
                                            $("input:checked").each(function()
                                            {
                                              list=list+$(this).val()+'|';
                                            });
                                    window.open('{$_subdomain}/LookupTables/satisfactionQuestionnaire/genreport/'+urlencode(list));        
                                    //$(location).attr('href', '{$_subdomain}/LookupTables/satisfactionQuestionnaire/genreport/'+urlencode(list));
                                    }
                                });

                    
                    $('#CResults').PCCSDataTable( {                  
                    sDom: 'Rft<"#dataTables_command">rpli', 
                    
                    "bServerSide": false,
                    "aoColumns": [                 
			{ "bVisible": false },
                        { "bVisible": false },
                        { "sWidth" :  "130px"},
                        { "sWidth" :  "130px"},
                        { "sWidth" :  "130px"},
                        { "sWidth" :  "80px"},
                             { "mData" : null, "bSortable": false, "sWidth" : "50px",
                            "mRender": function ( data, type, full ) {
                            return '<center><input class="taggedRec" type="checkbox" value="'+full[0]+'" name="check'+full[0]+'"></center>';
                             }
                          } 
			],               
                          
                            
                           
                            
                           
                            htmlTablePageId : 'CResultsPanel',
                            htmlTableId     : 'CResults',
                            fetchDataUrl    : '{$_subdomain}/LookupTables/ProcessData/Questionnaire/fetch/report/'+urlencode("{$bId}"),
                            formCancelButton: 'cancel_btn',
                           // pickCallbackMethod: 'openJob',
                           // dblclickCallbackMethod: 'gotoEditPage',
                            //fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            
                            iDisplayLength:  50,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError"


                        });
  
    });

    </script>

    {/block}
{block name=body}
 
    <div class="breadcrumb">
        <div>
        <a href="{$_subdomain}/index/index" >{$page['Text']['home_page']|escape:'html'}</a> / 
        <a href="{$_subdomain}/index/siteMap" >{$page['Text']['site_map']|escape:'html'}</a>  /         
        {$page['Text']['page_title_report']|escape:'html'}
        </div>
        
    </div>



    <div class="main" id="home" >

               <div class="LTTopPanel" >
                    <form id="bIdForm" style="margin-bottom: -53px;" >

                        <fieldset>
                        <legend title="" > {$page['Text']['page_title_report']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['report_description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 
                        <br /><br />
                        Brand :
                        <select name="bId" id="bId" >
                            <option value="" {if $bId eq ''}selected="selected"{/if}> Select Brand </option>
                             

                            {foreach $brands as $brand}

                                <option value="{$brand.BrandID}" {if $bId eq $brand.BrandID}selected="selected"{/if}>{$brand.BrandName|escape:'html'}</option>

                            {/foreach}
                

                                                    </select>


                    </form>
                </div>  


                <div class="LTResultsPanel" id="CResultsPanel" >

                    <table id="CResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
                                <tr>
                                        
                                        <th title="{$page['Text']['questionnaire_brandID']|escape:'html'}" >{$page['Text']['questionnaire_brandID']|escape:'html'}</th> 
                                        <th title="{$page['Labels']['brand_ID']|escape:'html'}" >{$page['Labels']['brand_ID']|escape:'html'}</th>
                                        <th title="{$page['Labels']['brand_name']|escape:'html'}" >{$page['Labels']['brand_name']|escape:'html'}</th>
                                        <th title="{$page['Text']['implementation_date']|escape:'html'}"  >{$page['Text']['implementation_date']|escape:'html'}</th>
                                        <th title="{$page['Labels']['termination_date']|escape:'html'}"  >{$page['Labels']['termination_date']|escape:'html'}</th>
                                        <th title="{$page['Text']['status']|escape:'html'}" >{$page['Text']['status']|escape:'html'}</th>
                                        <th title="{$page['Text']['select_report']|escape:'html'}" >{$page['Text']['select_report']|escape:'html'}</th>
                                        
                                </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>  
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['cancel']|escape:'html'}</span></button>
                   <button id="report_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['report']|escape:'html'}</span></button>
                </div>        


         


    </div>
                        
                        



{/block}


