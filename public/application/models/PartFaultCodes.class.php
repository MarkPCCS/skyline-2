<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of PartFaultCodes Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0 
 * @created     04/06/2013
 */
class PartFaultCodes extends CustomModel {

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);
        $this->SQLGen = $this->controller->loadModel('SQLGenerator');
        $this->fields = [
            "PartFaultCodeID",
            "FieldName",
            "ManufacturerID",
            "Status",
            "FieldNumber",
            "NonFaultCode",
            "MainOutFault",
            "KeyRepair",
            "CompulsoryAtCompletion",
            "CompulsoryForAdjustment",
            "FieldType",
            "Format",
            "ForceFormat",
            "CopyFromJobFaultCode",
            "JobFaultCodeNo",
            "RestrictLength",
            "LengthFrom",
            "LengthTo",
            "FillWithNAForAccessory",
            "FillWithNAForSoftwareUpgrade",
            "UseLookup",
            "ForceLookup"
            
        ];
    }

    public function insertPartFaultCodes($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
        $id = $this->SQLGen->dbInsert('part_fault_code', $this->fields, $P, true, true);
        return $id;
    }

    public function updatePartFaultCodes($P, $spid) {
        $P["ServiceProviderID"] = $spid;
        $P["Status"] = isset($P["Status"]) ? $P["Status"] : "Active";
        $id = $this->SQLGen->dbUpdate('part_fault_code', $this->fields, $P, "PartFaultCodeID=" . $P['PartFaultCodeID'], true);
    }

    public function getPartFaultCodesData($id) {
        $sql = "select * from part_fault_code where PartFaultCodeID=$id";
        $res = $this->query($this->conn, $sql);
        return $res[0];
    }

    public function deletePartFaultCodes($id) {
        $sql = "update part_fault_code set Status='In-Active' where PartFaultCodeID=$id";
        $this->execute($this->conn, $sql);
    }

    ////part_fault_code functions 

    public function checkIfOrderSection($id) {
        $sql = "select DisplayOrderSection from part_fault_code where PartStatusID=$id";
        $res = $this->query($this->conn, $sql);
        return $res[0]['DisplayOrderSection'];
    }

    public function addLinkedItem($p, $c) {
        $u = $this->controller->user->UserID;
        $sql = "insert into part_fault_code_lookup_to_part_fault_code  (PartFaultCodeID,PartFaultCodeLookupID,ModifiedUserID) values ($p,$c,$u)";
        $this->execute($this->conn, $sql);
    }

    public function delLinkedItem($p, $c) {
        $sql = "delete from part_fault_code_lookup_to_part_fault_code where PartFaultCodeID=$p and PartFaultCodeLookupID =$c";
        $this->execute($this->conn, $sql);
    }

    public function loadLinkedItem($p) {
        $u = $this->controller->user->UserID;
        if ($this->controller->user->SuperAdmin == 1) {
            $t = "part_fault_code_lookup";
            $m = "PartFaultCodeLookupID";
        } else {
            $t = "part_fault_code_lookup";
            $m = "PartFaultCodeLookupID";
        }
        $sql = "select am.PartFaultCodeLookupID, 
            LookupName as `ItemName`  from part_fault_code_lookup_to_part_fault_code am
                join $t m on m.$m=am.PartFaultCodeLookupID

            where PartFaultCodeID=$p";
        return $this->Query($this->conn, $sql);
    }

}

?>