# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.163');



# ---------------------------------------------------------------------- #
# Create table "unit_type_manufacturer"                                  #
# ---------------------------------------------------------------------- #
CREATE TABLE `unit_type_manufacturer` (
	`UnitTypeManufacturerID` INT(11) NOT NULL AUTO_INCREMENT,
	`UnitTypeID` INT(11) NOT NULL,
	`ManufacturerID` INT(11) NOT NULL,
	`ProductGroup` VARCHAR(10) NULL,
	`CreatedDate` TIMESTAMP NULL DEFAULT NULL,
	`EndDate` TIMESTAMP NULL,
	`Status` ENUM('Active','In-active') NOT NULL,
	`ModifiedUserID` INT(11) NULL,
	`ModifiedDate` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`UnitTypeManufacturerID`),
	INDEX `UnitTypeID` (`UnitTypeID`),
	INDEX `ManufacturerID` (`ManufacturerID`),
	CONSTRAINT `FK__unit_type` FOREIGN KEY (`UnitTypeID`) REFERENCES `unit_type` (`UnitTypeID`),
	CONSTRAINT `FK__manufacturer` FOREIGN KEY (`ManufacturerID`) REFERENCES `manufacturer` (`ManufacturerID`),
	CONSTRAINT `FK__user` FOREIGN KEY (`ModifiedUserID`) REFERENCES `user` (`UserID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;

# ---------------------------------------------------------------------- #
# Modify table "appointment"                                             #
# ---------------------------------------------------------------------- #
ALTER TABLE `appointment`
ADD COLUMN `ViamenteExclude` ENUM('Yes','No') NULL DEFAULT 'No' AFTER `Longitude`;

# ---------------------------------------------------------------------- #
# Modify table "non_skyline_job"                                         #
# ---------------------------------------------------------------------- #
ALTER TABLE `non_skyline_job`
ADD COLUMN `CustomerContactNumber` VARCHAR(50) NULL DEFAULT NULL AFTER `ReportedFault`,
DROP COLUMN `DeliveryAddress1`,
DROP COLUMN `DeliveryAddress2`,
DROP COLUMN `DeliveryAddress3`,
DROP COLUMN `DeliveryAddress4`,
DROP COLUMN `DeliveryPostcode`;


# ---------------------------------------------------------------------- #
# Modify table "service_provider_engineer"                               #
# ---------------------------------------------------------------------- #
ALTER TABLE `service_provider_engineer`
ADD COLUMN `ViamenteExclude` ENUM('Yes','No') NOT NULL DEFAULT 'No';


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.164');
