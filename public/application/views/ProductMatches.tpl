{extends "DemoLayout.tpl"}

{block name=config}
{$Title = "Multiple Matches"}
{$PageId = $ProductMatchesPage}
{/block}

{block name=scripts}
{*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
<script type="text/javascript" src="{$_subdomain}/js/product.form.validation.js"></script> 
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
{*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
<script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>

<script type="text/javascript" charset="utf-8">
function updateFn($sRow){ 
    
    //document.location = '{$_subdomain}/index/product/details/'+ $sRow[0] ;
    //window.open("{$_subdomain}/index/product/details/"+urlencode($sRow[0])) ;
    document.location = "{$_subdomain}/index/product/details/"+urlencode($sRow[0]);
    
}
function doublelclickFn($sRow){ 
    $('#product_PickButton').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
    $('#product_PickButton').trigger('click');
}
        {* updateDataUrl:      '{$_subdomain}/Data/product/update/',
        createDataUrl:      '{$_subdomain}/Data/product/create/',
        deleteDataUrl:      '{$_subdomain}/Data/product/delete/',
        createAppUrl:       '{$_subdomain}/index/product/display_create/',
        updateAppUrl:       '{$_subdomain}/index/product/display_update/',
        deleteAppUrl:       '{$_subdomain}/index/product/display_delete/',
        viewAppUrl:         '{$_subdomain}/index/product/display_view/', *}
$(document).ready(function() {
    $('#matches_table').PCCSDataTable( {
        aoColumns: [ 
            { bVisible: false },             
            { sWidth: '100px'}, 
            null, 
            null, 
            null,
            null],
        displayButtons:     'P',
        htmlTableId:        'matches_table',
        pickButtonId:        'product_PickButton',
        colorboxFormId:     'productForm',
        pickCallbackMethod: 'updateFn',
        dblclickCallbackMethod: 'doublelclickFn',
        fetchDataUrl:       '{$_subdomain}/Data/product/search/'+urlencode($('#productSearch').val()),
        parentURL:          '',
        searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
        frmErrorMsgClass: 'webformerror',
        frmErrorElement: 'label',
        isExistsDataUrl: '{$_subdomain}/Data/product/is_exists/',
        suggestTextId: 'suggestText',
        sugestFieldId: 'ProductID',
        frmErrorSugMsgClass: 'suggestwebformerror',
        hiddenPK: 'pk',
        passParamsType:'1', // 1 'means' parameters in query string.  Default 0;
        subdomain: '{$_subdomain}',
        superFilter : {$Product->super_filter},
        colorboxForceClose: false


    });
});//end document ready function
    
</script>
    
{/block}

{block name=body}

<div class="breadcrumb">
    <div>
        <a href="{$_subdomain}/index/index">Home Page</a> / Product Search
    </div>
</div>
    
{include file='include/menu.tpl'}
        
<div class="main" id="multiplematches">       
   
    <div class="SearchPanel">
        {include file='include/product_search.tpl'}
    </div> 
    {*for$Product->search_result*}            
   

    {*
    {if $Product->search_result != ''}
      *}   
     
    {if !$productNoResultsFlag}  
     <hr />
        
    <table id="matches_table" border="0" cellpadding="0" cellspacing="0" class="browse">
        <thead>
            <tr>
                {foreach $Product->display_columns as $col}
                    <th>{$col}</th>
                {/foreach}
            </tr>
        </thead>
        <tbody>
            <tr><td colspan="{$Product->display_columns|@count}" style="color:#666;">Loading....</td></tr> 
        </tbody>
    </table>
        
   {include file='include/button_panel.tpl'} 
   
   
   {/if}         
  
   
    {if $session_user_id && $productNoResultsFlag}
        <div class="SearchPanel">
        {include file='include/job_booking_panel.tpl'}
        </div>
    {/if}  
                                    
</div>
{/block}
