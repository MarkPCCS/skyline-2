<?xml version="1.0" encoding="UTF-8"?>
<PartOrder>
	<OrderHeader>
		<Company>{$nsp.CompanyCode}</Company>
		<RequestAccount>{$nsp.AccountNo}</RequestAccount>
		<ShipToAccount>4314094</ShipToAccount>
		<PurchaseNumber>{$partData.OrderNo}</PurchaseNumber>
		<PurchaseDate>{$smarty.now|date_format:"Ymd"}</PurchaseDate>
		<ShiptoLocation></ShiptoLocation>
		<POType></POType>
		<LanguageKey>E</LanguageKey>
		<ShipToAddress>
			<Name></Name>
			<Name_2></Name_2>
			<Street></Street>
			<City></City>
			<County></County>
			<Postcode></Postcode>
			<Region></Region>
			<CountryCode></CountryCode>
		</ShipToAddress>
	</OrderHeader>
	<OrderItemList>
            
                <OrderItem>
			<ReferenceNo>{$partData.PartID}</ReferenceNo>
			<Material>{$partData.PartNo}</Material>
			<CustomerMaterial>{$partData.PartNo}</CustomerMaterial>
			<Quantity>{$partData.Quantity}</Quantity>
			<TR_NO></TR_NO>
		</OrderItem>
           
		
	</OrderItemList>
</PartOrder>
