# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.169');

# ---------------------------------------------------------------------- #
# Add table "audit_new"                                                  #
# ---------------------------------------------------------------------- #
CREATE TABLE `audit_new` (
	`AuditID` INT(11) NOT NULL AUTO_INCREMENT,
	`UserID` INT(11) NOT NULL,
	`TableName` VARCHAR(100) NOT NULL,
	`Action` ENUM('update','delete') NOT NULL,
	`PrimaryID` VARCHAR(50) NOT NULL,
	`FieldName` VARCHAR(100) NULL DEFAULT NULL,
	`Value` TEXT NULL,
	`Date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (`AuditID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.170');
